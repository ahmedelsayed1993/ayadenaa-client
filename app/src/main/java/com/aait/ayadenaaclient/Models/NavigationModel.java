package com.aait.ayadenaaclient.Models;

/**
 * Created by Mahmoud on 10/22/17.
 */

public class NavigationModel {
    private int natIcon;

    private String navTitle;

    public NavigationModel(String navTitle, int natIcon) {
        this.navTitle = navTitle;
        this.natIcon = natIcon;
    }

    public int getNatIcon() {
        return natIcon;
    }
    public String getNavTitle() {
        return navTitle;
    }
}
