package com.aait.ayadenaaclient.Models;

import java.util.ArrayList;

public class TracksResponse extends BaseResponse {
    private ArrayList<TracksModel> data;

    public ArrayList<TracksModel> getData() {
        return data;
    }

    public void setData(ArrayList<TracksModel> data) {
        this.data = data;
    }
}
