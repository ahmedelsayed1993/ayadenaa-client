package com.aait.ayadenaaclient.Models;

import java.util.ArrayList;

public class BasketResponse extends BaseResponse {
    private ArrayList<CartResponse> data;

    public void setData(ArrayList<CartResponse> data) {
        this.data = data;
    }

    public ArrayList<CartResponse> getData() {
        return data;
    }
}
