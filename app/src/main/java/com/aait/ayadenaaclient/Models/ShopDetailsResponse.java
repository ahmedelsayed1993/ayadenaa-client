package com.aait.ayadenaaclient.Models;

public class ShopDetailsResponse extends BaseResponse {
    private ShopDetailsModel data;

    public ShopDetailsModel getData() {
        return data;
    }

    public void setData(ShopDetailsModel data) {
        this.data = data;
    }
}
