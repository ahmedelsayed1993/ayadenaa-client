package com.aait.ayadenaaclient.Models;

import java.util.ArrayList;

public class TrackResponse extends BaseResponse {
    private ArrayList<TrackModel> data;

    public ArrayList<TrackModel> getData() {
        return data;
    }

    public void setData(ArrayList<TrackModel> data) {
        this.data = data;
    }
}
