package com.aait.ayadenaaclient.Models;

public class RatesResponse extends BaseResponse {
    private CommentsResponse data;

    public CommentsResponse getData() {
        return data;
    }

    public void setData(CommentsResponse data) {
        this.data = data;
    }
}
