package com.aait.ayadenaaclient.Models;

import java.io.Serializable;

public class CategoriesModel implements Serializable {
    public CategoriesModel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    private int id;
    private String name;
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCategory_id() {
        return id;
    }

    public void setCategory_id(int category_id) {
        this.id = category_id;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }
}
