package com.aait.ayadenaaclient.Models;

public class ForgotPasswordResponse extends BaseResponse {
    private ForgetPasswordModel data;

    public ForgetPasswordModel getData() {
        return data;
    }

    public void setData(ForgetPasswordModel data) {
        this.data = data;
    }
}
