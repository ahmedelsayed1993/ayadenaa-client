package com.aait.ayadenaaclient.Models;

import java.util.ArrayList;

public class CategoriesResponse extends BaseResponse {
    private ArrayList<CategoriesModel> data;

    public ArrayList<CategoriesModel> getData() {
        return data;
    }

    public void setData(ArrayList<CategoriesModel> data) {
        this.data = data;
    }
}
