package com.aait.ayadenaaclient.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductResponse implements Serializable {
    private ArrayList<ShopProducts> products;

    public ArrayList<ShopProducts> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<ShopProducts> products) {
        this.products = products;
    }
}
