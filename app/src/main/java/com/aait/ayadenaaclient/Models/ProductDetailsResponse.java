package com.aait.ayadenaaclient.Models;

public class ProductDetailsResponse extends BaseResponse {
    private ProductDetailsModel data;

    public ProductDetailsModel getData() {
        return data;
    }

    public void setData(ProductDetailsModel data) {
        this.data = data;
    }
}
