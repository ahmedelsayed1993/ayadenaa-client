package com.aait.ayadenaaclient.Models;

import java.util.ArrayList;

public class ShopsResponse extends BaseResponse {
    private ArrayList<ShopModel> data;

    public ArrayList<ShopModel> getData() {
        return data;
    }

    public void setData(ArrayList<ShopModel> data) {
        this.data = data;
    }
}
