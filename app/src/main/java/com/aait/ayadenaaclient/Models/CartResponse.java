package com.aait.ayadenaaclient.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class CartResponse implements Serializable {
    private int cart_id;
    private int user_id;
    private int provider_id;
    private String provider_name;
    private String provider_image;
    private ArrayList<CartProductsModel> cart_products;
    private float total_price;
    private float delegate_price;

    public int getCart_id() {
        return cart_id;
    }

    public void setCart_id(int cart_id) {
        this.cart_id = cart_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(int provider_id) {
        this.provider_id = provider_id;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }

    public String getProvider_image() {
        return provider_image;
    }

    public void setProvider_image(String provider_image) {
        this.provider_image = provider_image;
    }

    public ArrayList<CartProductsModel> getCart_products() {
        return cart_products;
    }

    public void setCart_products(ArrayList<CartProductsModel> cart_products) {
        this.cart_products = cart_products;
    }

    public float getTotal_price() {
        return total_price;
    }

    public void setTotal_price(float total_price) {
        this.total_price = total_price;
    }

    public float getDelegate_price() {
        return delegate_price;
    }

    public void setDelegate_price(float delegate_price) {
        this.delegate_price = delegate_price;
    }
}
