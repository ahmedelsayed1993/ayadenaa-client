package com.aait.ayadenaaclient.Models;

import java.io.Serializable;

public class AdditionsModel implements Serializable {
    private int addition_id;
    private String addition_name;
    private String addition_price;
    private int count = 0;

    public int getNumber() {
        return count;
    }

    public void setNumber(final int number) {
        this.count = number;
    }

    public int getAddition_id() {
        return addition_id;
    }

    public void setAddition_id(int addition_id) {
        this.addition_id = addition_id;
    }

    public String getAddition_name() {
        return addition_name;
    }

    public void setAddition_name(String addition_name) {
        this.addition_name = addition_name;
    }

    public String getAddition_price() {
        return addition_price;
    }

    public void setAddition_price(String addition_price) {
        this.addition_price = addition_price;
    }
}
