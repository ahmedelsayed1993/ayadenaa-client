package com.aait.ayadenaaclient.Models;

import java.io.Serializable;

public class ListModel implements Serializable
{
    public ListModel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    private int id;
    private String name;
    private boolean isChecked = false;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
