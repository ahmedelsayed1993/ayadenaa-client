package com.aait.ayadenaaclient.Models;

public class ConfirmCodeModel extends BaseResponse {
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
