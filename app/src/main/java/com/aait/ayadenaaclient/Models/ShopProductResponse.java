package com.aait.ayadenaaclient.Models;

public class ShopProductResponse extends BaseResponse {
    private ProductResponse data;

    public ProductResponse getData() {
        return data;
    }

    public void setData(ProductResponse data) {
        this.data = data;
    }
}
