package com.aait.ayadenaaclient.Models;

import java.io.Serializable;

public class OrderModel implements Serializable {
    private int order_id;
    private int order_provider_id;
    private String order_provider_name;
    private String order_provider_avatar;
    private int order_delegate_id;
    private String order_delegate_name;
    private int order_way_type;
    private int order_status;
    private String order_refused_reason;

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getOrder_provider_id() {
        return order_provider_id;
    }

    public void setOrder_provider_id(int order_provider_id) {
        this.order_provider_id = order_provider_id;
    }

    public String getOrder_provider_name() {
        return order_provider_name;
    }

    public void setOrder_provider_name(String order_provider_name) {
        this.order_provider_name = order_provider_name;
    }

    public String getOrder_provider_avatar() {
        return order_provider_avatar;
    }

    public void setOrder_provider_avatar(String order_provider_avatar) {
        this.order_provider_avatar = order_provider_avatar;
    }

    public int getOrder_delegate_id() {
        return order_delegate_id;
    }

    public void setOrder_delegate_id(int order_delegate_id) {
        this.order_delegate_id = order_delegate_id;
    }

    public String getOrder_delegate_name() {
        return order_delegate_name;
    }

    public void setOrder_delegate_name(String order_delegate_name) {
        this.order_delegate_name = order_delegate_name;
    }

    public int getOrder_way_type() {
        return order_way_type;
    }

    public void setOrder_way_type(int order_way_type) {
        this.order_way_type = order_way_type;
    }

    public int getOrder_status() {
        return order_status;
    }

    public void setOrder_status(int order_status) {
        this.order_status = order_status;
    }

    public String getOrder_refused_reason() {
        return order_refused_reason;
    }

    public void setOrder_refused_reason(String order_refused_reason) {
        this.order_refused_reason = order_refused_reason;
    }
}
