package com.aait.ayadenaaclient.Models;

import java.util.ArrayList;

public class PriceResponse extends BaseResponse {
    private ArrayList<PriceModel> data;

    public ArrayList<PriceModel> getData() {
        return data;
    }

    public void setData(ArrayList<PriceModel> data) {
        this.data = data;
    }
}
