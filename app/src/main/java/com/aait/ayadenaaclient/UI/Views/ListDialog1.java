package com.aait.ayadenaaclient.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.ayadenaaclient.Listener.OnItemClickListener;
import com.aait.ayadenaaclient.Models.ListModel;
import com.aait.ayadenaaclient.Models.PriceModel;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.UI.Adapters.ListDialogAdapter;
import com.aait.ayadenaaclient.UI.Adapters.ListDialogAdapter1;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListDialog1 extends Dialog {

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.lay_no_data)
    LinearLayout lay_no_data;

    @BindView(R.id.tv_title)
    TextView tv_title;

    LinearLayoutManager mLinearLayoutManager;

    Context mContext;

    OnItemClickListener onItemClickListener;



    ListDialogAdapter1 mListAdapter;

    String title;
    List<PriceModel> priceModels;



    public ListDialog1(Context mContext, OnItemClickListener onItemClickListener, List<PriceModel> priceModels,
                      String title) {
        super(mContext);
        this.mContext = mContext;
        this.priceModels = priceModels;
        this.onItemClickListener = onItemClickListener;
        this.title = title;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_custom_layout);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(false);
        ButterKnife.bind(this);
        initializeComponents();
    }

    private void initializeComponents() {
        tv_title.setText(title);
        mLinearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(mLinearLayoutManager);
        mListAdapter = new ListDialogAdapter1(mContext, priceModels);
        mListAdapter.setOnItemClickListener(onItemClickListener);
        rvRecycle.setAdapter(mListAdapter);

        if (priceModels.size() == 0) {
            lay_no_data.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.iv_close)
    void onCloseClicked() {
        dismiss();
    }
}

