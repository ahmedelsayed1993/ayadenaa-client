package com.aait.ayadenaaclient.UI.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.ayadenaaclient.App.Constant.BundleData;
import com.aait.ayadenaaclient.Models.AdditionsModel;
import com.aait.ayadenaaclient.Models.AddtionModel;
import com.aait.ayadenaaclient.Models.ProductDetailsModel;
import com.aait.ayadenaaclient.Models.ProductDetailsResponse;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.Network.Urls;
import com.aait.ayadenaaclient.R;

import com.aait.ayadenaaclient.Base.ParentActivity;
import com.aait.ayadenaaclient.Listener.OnItemClickListener;
import com.aait.ayadenaaclient.Models.BaseResponse;
import com.aait.ayadenaaclient.UI.Adapters.SpecialAdditiveAdapter;
import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud on 2/17/18.
 */

public class OrderItemActivity extends ParentActivity implements OnItemClickListener {

    @BindView(R.id.iv_image)
    ImageView ivImage;

    @BindView(R.id.tv_food_name)
    TextView tvFoodName;

    @BindView(R.id.tv_normal_price)
    TextView tvNormalPrice;

    @BindView(R.id.tv_discount_price)
    TextView tvDiscountPrice;

    @BindView(R.id.tv_description)
    TextView tvDescription;


    @BindView(R.id.tv_offer_image)
    ImageView tv_offer_image;


    @BindView(R.id.iv_minus)
    ImageView ivMinus;

    @BindView(R.id.iv_add)
    ImageView ivAdd;

    @BindView(R.id.ed_order_number)
    EditText edOrderNumber;


    @BindView(R.id.rv_Recycle)
    RecyclerView rvRecycle;

    LinearLayoutManager linearLayoutManager;

    SpecialAdditiveAdapter mSpecialAdditiveAdapter;

    List<AdditionsModel> mSpecialAdditiveModels = new ArrayList<>();

    ProductDetailsModel mFoodModel;
    String id;

////////

    @Override
    protected void initializeComponents() {
        id =  getIntent().getStringExtra("id");

        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mSpecialAdditiveAdapter = new SpecialAdditiveAdapter(mContext, mSpecialAdditiveModels,
                R.layout.recycle_special_adictive);
        mSpecialAdditiveAdapter.setOnItemClickListener(this);
        rvRecycle.setAdapter(mSpecialAdditiveAdapter);
        if (mSharedPrefManager.getLoginStatus()) {
            showProduct(mSharedPrefManager.getUserData().getUser_id());
        }else {
            showProduct(0);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_item;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @OnClick(R.id.iv_minus)
    void onMinusClick() {
        if (Integer.parseInt(edOrderNumber.getText().toString()) == 1) {

        } else {
            edOrderNumber.setText((Integer.parseInt(edOrderNumber.getText().toString()) - 1) + "");
            int price = Integer.parseInt(mFoodModel.getProduct_price());
            int price2 = Integer.parseInt(mFoodModel.getProduct_offer());
            String price1 =price*Integer.parseInt(edOrderNumber.getText().toString())+"";
            String price3 = price2*Integer.parseInt(edOrderNumber.getText().toString())+"";
            tvNormalPrice.setText(price1+ " " + getResources().getString(R.string.SAR));
            tvDiscountPrice.setText(price3+" "+getResources().getString(R.string.SAR));
        }
    }

    @OnClick(R.id.iv_add)
    void onAddClick() {

        edOrderNumber.setText((Integer.parseInt(edOrderNumber.getText().toString()) + 1) + "");
        int price = Integer.parseInt(mFoodModel.getProduct_price());
        int price2 = Integer.parseInt(mFoodModel.getProduct_offer());
        String price1 =price*Integer.parseInt(edOrderNumber.getText().toString())+"";
        String price3 = price2*Integer.parseInt(edOrderNumber.getText().toString())+"";
        tvNormalPrice.setText(price1+ " " + getResources().getString(R.string.SAR));
        tvDiscountPrice.setText(price3+" "+getResources().getString(R.string.SAR));
    }

    @OnClick(R.id.btn_add_to_card)
    void onBtnAddToCardClick() {
        ArrayList<AddtionModel> addtionModel = new ArrayList<>();
        for (int i=0;i<mSpecialAdditiveModels.size();i++) {
            AddtionModel addtionModel1;

            addtionModel1 = new AddtionModel(mSpecialAdditiveModels.get(i).getAddition_id(),mSpecialAdditiveModels.get(i).getNumber());
            addtionModel.add(addtionModel1);

        }
        CommonUtil.PrintLogE(new Gson().toJson(addtionModel));
        if (mSharedPrefManager.getLoginStatus()) {
            AddToCart(mFoodModel.getProduct_id(), Integer.parseInt(edOrderNumber.getText().toString()), mSharedPrefManager.getUserData().getUser_id(), new Gson().toJson(addtionModel));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.must_login));
        }
//        CommonUtil.PrintLogE(new Gson().toJson(mSpecialAdditiveModels));
//        addItem(Urls.BASE_TOKEN + mSharedPrefManager.getUserData().getJwt_token(), mFoodModel.getId(),
//                Integer.parseInt(edOrderNumber.getText().toString()), new Gson().toJson(mSpecialAdditiveModels));
    }

    @Override
    public void onItemClick(final View view, final int position) {
        if (view.getId() == R.id.iv_minus) {
            if (mSpecialAdditiveModels.get(position).getNumber() == 0) {

            } else {
                mSpecialAdditiveModels.get(position).setNumber(mSpecialAdditiveModels.get(position).getNumber() - 1);
                mSpecialAdditiveAdapter.notifyDataSetChanged();
            }
        } else if (view.getId() == R.id.iv_add) {
            mSpecialAdditiveModels.get(position).setNumber(mSpecialAdditiveModels.get(position).getNumber() + 1);
            mSpecialAdditiveAdapter.notifyDataSetChanged();
        }
    }

    void setData(ProductDetailsModel productDetailsModel) {
        setToolbarTitle(productDetailsModel.getProduct_name());
        Glide.with(mContext).load(productDetailsModel.getProduct_image()).asBitmap().placeholder(R.mipmap.splash)
                .into(ivImage);
        tvFoodName.setText(productDetailsModel.getProduct_name());
        int price =Integer.parseInt(productDetailsModel.getProduct_price()) ;
        String price1 =price*Integer.parseInt(edOrderNumber.getText().toString())+"";
        Log.e("aaaaaaaaa",price1);
        tvNormalPrice
                .setText((price1) + " " + getResources().getString(R.string.SAR));
        if (productDetailsModel.isProduct_have_offer()==true) {
            CommonUtil.setStrokInText(tvNormalPrice);
            tvDiscountPrice
                    .setText(productDetailsModel.getProduct_offer() + " " + getResources()
                            .getString(R.string.SAR));
        } else {
            tvDiscountPrice.setVisibility(View.GONE);
            tv_offer_image.setVisibility(View.GONE);
        }

        tvDescription.setText(productDetailsModel.getProduct_disc());
    }

    private void AddToCart(int Product_id,int cout,int user_id,String additions){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).addToCart(mLanguagePrefManager.getAppLanguage(),user_id,Product_id,cout,additions).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext,BasketActivity.class));
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });

    }
    private void showProduct(int user_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).showproduct(user_id,Integer.parseInt(id)).enqueue(new Callback<ProductDetailsResponse>() {
            @Override
            public void onResponse(Call<ProductDetailsResponse> call, Response<ProductDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        setData(response.body().getData());
                        mFoodModel = response.body().getData();
                        mSpecialAdditiveAdapter.updateAll(response.body().getData().getProduct_additions());
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
