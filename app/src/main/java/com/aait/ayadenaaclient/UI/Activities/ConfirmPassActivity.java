package com.aait.ayadenaaclient.UI.Activities;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.aait.ayadenaaclient.App.Constant;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.aait.ayadenaaclient.Uitls.ValidationUtils;
import com.aait.ayadenaaclient.Base.ParentActivity;
import com.aait.ayadenaaclient.Models.NewPasswordResponse;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmPassActivity extends ParentActivity {

    @BindView(R.id.til_code)
    TextInputLayout tilCode;

    @BindView(R.id.et_code)
    TextInputEditText etCode;
    String forgetPassModel;
    String reg ;
    String type;

    public static void startActivityNewPass(AppCompatActivity mAppCompatActivity, String forgetPassModel, String type) {
        Intent mIntent = new Intent(mAppCompatActivity, ConfirmPassActivity.class);
        mIntent.putExtra(Constant.BundleData.FORGET_PASS_MODEL,forgetPassModel);
        mIntent.putExtra("type",type);
        mIntent.putExtra(Constant.BundleData.FAMILY_MODEL,forgetPassModel);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData() {
      //  user_id = (String) getIntent().getSerializableExtra("user_id");
        forgetPassModel = (String) getIntent().getSerializableExtra(Constant.BundleData.FORGET_PASS_MODEL);
        type = (String) getIntent().getSerializableExtra("type");
    }
    @Override
    protected void initializeComponents() {
       getBundleData();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_confirm_account;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    boolean confirmCodeValidation() {
        if (!ValidationUtils.checkError(etCode, tilCode, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }
    @OnClick(R.id.btn_register)
    void onBtnRegisterClick() {
        if (confirmCodeValidation()){
//         if (!forgetPassModel.equals("")) {
//            confirmnewpass();
//         }else {
            confirmnewpass();
            // }
        }
    }
    private void confirmnewpass(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).forgetPassword(forgetPassModel,etCode.getText().toString())
                .enqueue(new Callback<NewPasswordResponse>() {
                    @Override
                    public void onResponse(Call<NewPasswordResponse> call, Response<NewPasswordResponse> response) {
                        hideProgressDialog();
                        Log.e("nnn",new Gson().toJson(response.body().getData()));
                        if (response.isSuccessful()){
                            if (response.body().getStatus()==1){
                                NewPasswordActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getUser_id()+"",type);

                            }else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<NewPasswordResponse> call, Throwable t) {
                        Log.e("mmm",new Gson().toJson(t));
                        CommonUtil.handleException(mContext,t);
                        t.printStackTrace();
                        hideProgressDialog();

                    }
                });
    }
}
