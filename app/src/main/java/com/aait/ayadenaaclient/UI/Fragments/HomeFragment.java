package com.aait.ayadenaaclient.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;

//import com.aait.ayadenaclient.Network.RetroWeb;
//import com.aait.ayadenaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.Models.CategoriesModel;
import com.aait.ayadenaaclient.Models.CategoriesResponse;
import com.aait.ayadenaaclient.Models.ListModelResponse;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;
//import com.aait.ayadenaclient.UI.Activities.FamiliesActivity;
//import com.aait.ayadenaclient.UI.Adapters.CategoriesAdapter;
import com.aait.ayadenaaclient.Base.BaseFragment;
import com.aait.ayadenaaclient.Listener.OnItemClickListener;
//import com.aait.ayadenaclient.models.CategoryModel;
import com.aait.ayadenaaclient.UI.Activities.FamiliesActivity;
import com.aait.ayadenaaclient.UI.Adapters.CategoriesAdapter;
import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */

public class HomeFragment extends BaseFragment implements OnItemClickListener {

    CategoriesAdapter mCategoriesAdapter;
//
   ArrayList<CategoriesModel> categoriesModels = new ArrayList<>();

    GridLayoutManager layoutManager;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initializeComponents(final View view) {

        layoutManager = new GridLayoutManager(mContext, 2);
        mCategoriesAdapter = new CategoriesAdapter(mContext, categoriesModels, R.layout.recycle_categories);
        mCategoriesAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(layoutManager);
        rvRecycle.setItemAnimator(new DefaultItemAnimator());
        rvRecycle.setAdapter(mCategoriesAdapter);

        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCategories(mLanguagePrefManager.getAppLanguage());
            }
        });

        getCategories(mLanguagePrefManager.getAppLanguage());
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(final View view, final int position) {
        Log.e("id",categoriesModels.get(position).getCategory_id()+"");
        Intent intent = new Intent(mContext,FamiliesActivity.class);
        intent.putExtra("id",categoriesModels.get(position).getCategory_id()+"");
        intent.putExtra("name",categoriesModels.get(position).getname());
        startActivity(intent);
      //  FamiliesActivity.startActivity((AppCompatActivity) mContext, mCategoryModels.get(position).getCategory_id());
    }

    private void getCategories(String lang) {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getCategory(lang)
                .enqueue(new Callback<CategoriesResponse>() {
                    @Override
                    public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            Log.e("hhh",new Gson().toJson(response.body().getData()));
                            categoriesModels = response.body().getData();
                            if (response.body().getData().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                mCategoriesAdapter.updateAll(response.body().getData());
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
}

