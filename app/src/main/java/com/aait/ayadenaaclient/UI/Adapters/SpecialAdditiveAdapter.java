package com.aait.ayadenaaclient.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaclient.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaclient.Models.AdditionsModel;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Mahmoud ibrahim on 12/2/2018.
 */

public class SpecialAdditiveAdapter extends ParentRecyclerAdapter<AdditionsModel> {

    public SpecialAdditiveAdapter(final Context context, final List<AdditionsModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        AdditionsModel specialAdditiveModel = data.get(position);
        viewHolder.add.setVisibility(View.VISIBLE);
        viewHolder.tvPrice
                .setText(specialAdditiveModel.getAddition_price() + " " + mcontext.getResources().getString(R.string.SAR));
        viewHolder.tvSpecialAdditiveName.setText(specialAdditiveModel.getAddition_name());

        viewHolder.edOrderNumber.setText(specialAdditiveModel.getNumber() + "");
        viewHolder.ivAdd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View view) {
                itemClickListener.onItemClick(view, position);
            }
        });
        viewHolder.ivMinus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View view) {
                itemClickListener.onItemClick(view, position);
            }
        });

    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_price)
        TextView tvPrice;

        @BindView(R.id.tv_special_additive_name)
        TextView tvSpecialAdditiveName;

        @BindView(R.id.iv_minus)
        ImageView ivMinus;

        @BindView(R.id.ed_order_number)
        EditText edOrderNumber;

        @BindView(R.id.iv_add)
        ImageView ivAdd;
        @BindView(R.id.add)
        LinearLayout add;


        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}
