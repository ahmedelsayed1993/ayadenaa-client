package com.aait.ayadenaaclient.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.ayadenaaclient.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaclient.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaclient.Models.CartAdditions;
import com.aait.ayadenaaclient.R;

import java.util.List;

import butterknife.BindView;

public class FamilyOrderSpecialAdditiveAdapter extends ParentRecyclerAdapter<CartAdditions> {

    private int mRowIndex = -1;

    public void setData(List<CartAdditions> modelData) {
        if (data != modelData) {
            data = modelData;
            notifyDataSetChanged();
        }
    }

    public void setRowIndex(int index) {
        mRowIndex = index;
    }

    public FamilyOrderSpecialAdditiveAdapter(final Context context) {
        super(context);
    }


    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext)
                .inflate(R.layout.recycle_special_adictive, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        CartAdditions specialAdditiveModel = data.get(position);
        viewHolder.tvAdditionPrice
                .setText(specialAdditiveModel.getAddition_price() + " " + mcontext.getResources().getString(R.string.SAR));
        viewHolder.tvAdditionName.setText(specialAdditiveModel.getAddition_name());
        viewHolder.tvAdditionNumber.setText(specialAdditiveModel.getAddition_count() + " ");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_price)
        TextView tvAdditionPrice;

        @BindView(R.id.tv_special_additive_name)
        TextView tvAdditionName;

        @BindView(R.id.ed_order_number)
        TextView tvAdditionNumber;

        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}
