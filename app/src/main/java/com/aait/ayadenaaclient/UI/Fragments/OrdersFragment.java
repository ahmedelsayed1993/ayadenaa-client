package com.aait.ayadenaaclient.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

//import com.aait.ayadenaclient.Network.RetroWeb;
//import com.aait.ayadenaclient.Network.ServiceApi;
//import com.aait.ayadenaclient.Network.Urls;
import com.aait.ayadenaaclient.Models.BaseResponse;
import com.aait.ayadenaaclient.Models.OrderModel;
import com.aait.ayadenaaclient.Models.OrderResponse;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;
//import com.aait.ayadenaclient.UI.Activities.ProcessedOrderDetailsActiity;
//import com.aait.ayadenaclient.UI.Adapters.NewOrderAdapter;
import com.aait.ayadenaaclient.Base.BaseFragment;
import com.aait.ayadenaaclient.Listener.OnItemClickListener;
import com.aait.ayadenaaclient.Listener.PaginationAdapterCallback;
import com.aait.ayadenaaclient.Listener.PaginationScrollListener;
//import com.aait.ayadenaclient.models.getOrdersResponse.OrderModel;
//import com.aait.ayadenaclient.models.getOrdersResponse.getOrdersResponse;
import com.aait.ayadenaaclient.UI.Activities.MainActivity;
import com.aait.ayadenaaclient.UI.Adapters.NewOrderAdapter;
import com.aait.ayadenaaclient.Uitls.CommonUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */

public class OrdersFragment extends BaseFragment implements OnItemClickListener, PaginationAdapterCallback {

    LinearLayoutManager linearLayoutManager;

    NewOrderAdapter mNewOrderAdapter;

    List<OrderModel> mOrderModels = new ArrayList<>();

    private static final int PAGE_START = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;

    public static OrdersFragment newInstance() {
        Bundle args = new Bundle();
        OrdersFragment fragment = new OrdersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_orders;
    }

    @Override
    protected void initializeComponents(final View view) {
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mNewOrderAdapter = new NewOrderAdapter(mContext, mOrderModels);
        mNewOrderAdapter.setOnItemClickListener(this);
        mNewOrderAdapter.setOnPaginationClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setItemAnimator(new DefaultItemAnimator());
        rvRecycle.setAdapter(mNewOrderAdapter);
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

        });
        if (mSharedPrefManager.getLoginStatus()) {

            swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getNewOrder(mSharedPrefManager.getUserData().getUser_id());
                }
            });

            getNewOrder(mSharedPrefManager.getUserData().getUser_id());
        }else {
            layNoInternet.setVisibility(View.GONE);
            layNoItem.setVisibility(View.VISIBLE);
            layProgress.setVisibility(View.GONE);
            swipeRefresh.setRefreshing(false);
        }
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(final View view, final int position) {
       if (view.getId()==R.id.receive){

           change(mOrderModels.get(position).getOrder_id());
       }
    }

    @Override
    public void retryPageLoad() {

    }


    private void getNewOrder(int user_id) {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getClientOrders(mLanguagePrefManager.getAppLanguage(), user_id)
                .enqueue(new Callback<OrderResponse>() {
                    @Override
                    public void onResponse(Call<OrderResponse> call,
                            Response<OrderResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getData().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                mNewOrderAdapter.updateAll(response.body().getData());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<OrderResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
    private void change(int order_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).changeStatus(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),order_id,8).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,getString(R.string.order_is_received));
                        startActivity(new Intent(mContext, MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
