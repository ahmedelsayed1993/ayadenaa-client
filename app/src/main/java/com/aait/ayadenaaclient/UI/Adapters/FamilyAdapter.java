package com.aait.ayadenaaclient.UI.Adapters;


import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.ayadenaaclient.App.Constant;
import com.aait.ayadenaaclient.Models.ShopModel;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaclient.Base.ParentRecyclerViewHolder;

import com.bumptech.glide.Glide;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */
public class FamilyAdapter extends ParentRecyclerAdapter<ShopModel> {

    private String errorMsg;

    public FamilyAdapter(final Context context, final List<ShopModel> data) {
        super(context, data);
    }


    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        switch (viewType) {
            case Constant.InfinitScroll.ITEM:
                View viewItem = inflater.inflate(R.layout.recycle_families_row, parent, false);
                viewHolder = new FamileyViewHolder(viewItem);
                break;
            case Constant.InfinitScroll.LOADING:
                View viewLoading = inflater.inflate(R.layout.recycle_item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case Constant.InfinitScroll.ITEM:
                final FamileyViewHolder famileyViewHolder = (FamileyViewHolder) holder;
                ShopModel familyModel = data.get(position);
                Glide.with(mcontext).load(familyModel.getShop_user_image()).asBitmap().placeholder(R.mipmap.splash)
                        .into(famileyViewHolder.ivCatImage);
                famileyViewHolder.tvFamileyName.setText(familyModel.getShop_user_name());
                famileyViewHolder.tvCityName.setText(
                         familyModel.getShop_city());
                famileyViewHolder.rbRating.setRating(familyModel.getShop_rate());

                famileyViewHolder.itemView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        itemClickListener.onItemClick(view, position);
                    }
                });

                if (familyModel.getShop_categories() != null) {
                    if (familyModel.getShop_categories().size() >= 3) {
                        famileyViewHolder.tvCategoryOne
                                .setText(familyModel.getShop_categories().get(0).getName());
                        famileyViewHolder.tvCategoryTwo
                                .setText(familyModel.getShop_categories().get(1).getName());
                        famileyViewHolder.tvCategoryThree
                                .setText(familyModel.getShop_categories().get(2).getName());
                    } else if (familyModel.getShop_categories().size() == 2) {
                        famileyViewHolder.tvCategoryOne
                                .setText(familyModel.getShop_categories().get(0).getName());
                        famileyViewHolder.tvCategoryTwo
                                .setText(familyModel.getShop_categories().get(1).getName());
                        famileyViewHolder.tvCategoryThree.setVisibility(View.GONE);
                    } else if (familyModel.getShop_categories().size() == 1) {
                        famileyViewHolder.tvCategoryOne
                                .setText(familyModel.getShop_categories().get(0).getName());
                        famileyViewHolder.tvCategoryTwo.setVisibility(View.GONE);
                        famileyViewHolder.tvCategoryThree.setVisibility(View.GONE);
                    } else if (familyModel.getShop_categories().size() == 0) {
                        famileyViewHolder.tvCategoryOne.setVisibility(View.GONE);
                        famileyViewHolder.tvCategoryTwo.setVisibility(View.GONE);
                        famileyViewHolder.tvCategoryThree.setVisibility(View.GONE);
                    }
                }
                break;

            case Constant.InfinitScroll.LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    mcontext.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == data.size() - 1 && isLoadingAdded)
                ? Constant.InfinitScroll.LOADING
                : Constant.InfinitScroll.ITEM;
    }


    /**
     * Main list's content ViewHolder
     */

    protected class FamileyViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.iv_cat_image)
        CircleImageView ivCatImage;

        @BindView(R.id.tv_familey_name)
        TextView tvFamileyName;

        @BindView(R.id.tv_city_name)
        TextView tvCityName;

        @BindView(R.id.rb_rating)
        AppCompatRatingBar rbRating;

        @BindView(R.id.tv_category_one)
        TextView tvCategoryOne;

        @BindView(R.id.tv_category_two)
        TextView tvCategoryTwo;

        @BindView(R.id.tv_category_three)
        TextView tvCategoryThree;

        public FamileyViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }


    protected class LoadingVH extends ParentRecyclerViewHolder {


        @BindView(R.id.progress_wheel)
        ProgressWheel mProgressBar;

        @BindView(R.id.loadmore_retry)
        ImageButton mRetryBtn;

        @BindView(R.id.loadmore_errortxt)
        TextView mErrorTxt;

        @BindView(R.id.loadmore_errorlayout)
        LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            ProgressWheel mProgressBar = (ProgressWheel) findViewById(R.id.progress_wheel);
        }

        @OnClick(R.id.loadmore_retry)
        void onLoadMoreRetryClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }

        @OnClick(R.id.loadmore_errorlayout)
        void onLoadMoreErrorLayoutClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }
    }


    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(data.size() - 1);
        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }


}
