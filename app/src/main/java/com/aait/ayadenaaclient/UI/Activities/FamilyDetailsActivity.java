package com.aait.ayadenaaclient.UI.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.ayadenaaclient.App.Constant.BundleData;
import com.aait.ayadenaaclient.Models.ShopCategories;
import com.aait.ayadenaaclient.Models.ShopDetailsModel;
import com.aait.ayadenaaclient.Models.ShopDetailsResponse;
import com.aait.ayadenaaclient.Models.ShopProductResponse;
import com.aait.ayadenaaclient.Models.ShopProducts;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.Network.Urls;
import com.aait.ayadenaaclient.R;

import com.aait.ayadenaaclient.UI.Adapters.FamilyCategoriesAdapter;
import com.aait.ayadenaaclient.UI.Adapters.OffersAdapter;
import com.aait.ayadenaaclient.Base.ParentActivity;
import com.aait.ayadenaaclient.Listener.OnItemClickListener;

import com.aait.ayadenaaclient.UI.Adapters.OffersAdapter1;

import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */

public class FamilyDetailsActivity extends ParentActivity implements OnItemClickListener {

    @BindView(R.id.tv_family_name)
    TextView tvFamilyName;

    @BindView(R.id.tv_city)
    TextView tvCity;

    @BindView(R.id.rb_rating)
    AppCompatRatingBar rbRating;

    @BindView(R.id.rv_recycle_categores)
    RecyclerView rvRecycleCategores;



    @BindView(R.id.civ_image)
    CircleImageView civ_image;

    @BindView(R.id.iv_background)
    ImageView Background;

    @BindView(R.id.btn_rate)
    Button btnRate;


    LinearLayoutManager linearLayoutManager;

    FamilyCategoriesAdapter mFamilyCategoriesAdapter;

    List<ShopCategories> mCategoryModels = new ArrayList<>();


    LinearLayoutManager linearLayoutManagerVertical;

    OffersAdapter1 mOffersAdapter;

    List<ShopProducts> mFoodModels = new ArrayList<>();

    ShopDetailsModel familyModel;

    int selectedPosition;

//    public static void startActivity(AppCompatActivity mAppCompatActivity, FamilyModel familyModel) {
//        Intent mIntent = new Intent(mAppCompatActivity, FamilyDetailsActivity.class);
//        mIntent.putExtra(BundleData.FAMILY_MODEL, familyModel);
//        mAppCompatActivity.startActivity(mIntent);
//    }

//    void getBundleData() {
//        familyModel = (FamilyModel) getIntent().getSerializableExtra(BundleData.FAMILY_MODEL);
//        mCategoryModels = familyModel.getCategories();
//        mCategoryModels.get(0).setChecked(true);
//    }
String id;
    @Override
    protected void initializeComponents() {

        id = getIntent().getStringExtra("id");
       // getBundleData();

        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        rvRecycleCategores.setLayoutManager(linearLayoutManager);
        mFamilyCategoriesAdapter = new FamilyCategoriesAdapter(mContext, mCategoryModels,
                R.layout.recycle_family_categories_row);
        mFamilyCategoriesAdapter.setOnItemClickListener(this);
        rvRecycleCategores.setAdapter(mFamilyCategoriesAdapter);

        linearLayoutManagerVertical = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManagerVertical);
        mOffersAdapter = new OffersAdapter1(mContext, mFoodModels);
        mOffersAdapter.setOnItemClickListener(this);
        rvRecycle.setAdapter(mOffersAdapter);
        if (mSharedPrefManager.getLoginStatus()) {
            getShopData(mSharedPrefManager.getUserData().getUser_id());


            swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (mCategoryModels.size() != 0) {
                        mCategoryModels.get(0).setChecked(true);
                        getProductCategories(mCategoryModels.get(0).getCategory_id(), mSharedPrefManager.getUserData().getUser_id()
                        );
                    }

                }
            });

            if (mCategoryModels.size() != 0) {
                mCategoryModels.get(0).setChecked(true);
                getProductCategories(mCategoryModels.get(0).getCategory_id(), mSharedPrefManager.getUserData().getUser_id()
                );
            }
        }else {
            getShopData(0);


            swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (mCategoryModels.size() != 0) {
                        mCategoryModels.get(0).setChecked(true);
                        getProductCategories(mCategoryModels.get(0).getCategory_id(), 0
                        );
                    }

                }
            });

            if (mCategoryModels.size() != 0) {
                mCategoryModels.get(0).setChecked(true);
                getProductCategories(mCategoryModels.get(0).getCategory_id(), 0
                );
            }
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_family_store_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @OnClick(R.id.btn_rate)
    void onRateClick() {
        RateActivity.startActivity((AppCompatActivity) mContext, Integer.parseInt(id));
    }

    @Override
    public void onItemClick(final View view, final int position) {
        if (view.getId() == R.id.tv_categories) {
            // click on categories section
            CommonUtil.PrintLogE("categories click");
            selectedPosition = position;
            if (!mCategoryModels.get(selectedPosition).isChecked()) {
                for (int i = 0; i < mCategoryModels.size(); i++) {
                    mCategoryModels.get(i).setChecked(false);
                }

                mCategoryModels.get(selectedPosition).setChecked(true);
                mFamilyCategoriesAdapter.notifyDataSetChanged();
                if (mSharedPrefManager.getLoginStatus()) {
                    getProductCategories(
                            mCategoryModels.get(selectedPosition).getCategory_id(), mSharedPrefManager.getUserData().getUser_id()
                    );
                }else {
                    getProductCategories(
                            mCategoryModels.get(selectedPosition).getCategory_id(), 0
                    );
                }
            } else {
                CommonUtil.PrintLogE("Selected before .... :) (:");
            }
        } else {
            // click on food item
            Intent intent = new Intent(mContext,OrderItemActivity.class);
            intent.putExtra("id",mFoodModels.get(position).getProduct_id()+"");
            startActivity(intent);
           // OrderItemActivity.startActivity((AppCompatActivity) mContext, mFoodModels.get(position));
        }
    }


    private void getProductCategories( int categoryId,int user_id) {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getProducts(user_id, Integer.parseInt(id), categoryId, mLanguagePrefManager.getAppLanguage())
                .enqueue(new Callback<ShopProductResponse>() {
                    @Override
                    public void onResponse(Call<ShopProductResponse> call,
                            Response<ShopProductResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getData().getProducts().size() == 0) {
                                layNoItem.setVisibility(View.VISIBLE);
                                layNoInternet.setVisibility(View.GONE);
                                tvNoContent.setText(R.string.no_data);
                            } else {
                                mOffersAdapter.updateAll(response.body().getData().getProducts());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ShopProductResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }
private void getShopData(final int user_id){
    showProgressDialog(getString(R.string.please_wait));
    RetroWeb.getClient().create(ServiceApi.class).showShop(mLanguagePrefManager.getAppLanguage(),user_id,Integer.parseInt(id)).
            enqueue(new Callback<ShopDetailsResponse>() {
                @Override
                public void onResponse(Call<ShopDetailsResponse> call, Response<ShopDetailsResponse> response) {
                    hideProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getStatus()==1){
                            setData(response.body().getData());
//                            tvCity.setText(response.body().getData().getUser_city());
//                            tvFamilyName.setText(response.body().getData().getShop_name());
//                            rbRating.setRating(response.body().getData().getShop_rate());
//                            Glide.with(mContext).load(response.body().getData().getUser_image()).asBitmap().placeholder(R.mipmap.logo).into(civ_image);
//                            Glide.with(mContext).load(response.body().getData().getUser_image()).asBitmap().placeholder(R.mipmap.logo).into(Background);
                            mCategoryModels = response.body().getData().getUser_categories();
                            mFamilyCategoriesAdapter.updateAll(response.body().getData().getUser_categories());
                            getProductCategories(response.body().getData().getUser_categories().get(0).getCategory_id(),user_id);
//                            mOffersAdapter.updateAll(response.body().getData().getProducts());
//                            mFoodModels = response.body().getData().getProducts();
                        }
                        else {
                            CommonUtil.makeToast(mContext,response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ShopDetailsResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext,t);
                    t.printStackTrace();
                    hideProgressDialog();

                }
            });
}

    void setData(ShopDetailsModel familyModel) {
        setToolbarTitle(familyModel.getShop_name());
        mCategoryModels = familyModel.getUser_categories();
        mCategoryModels.get(0).setChecked(true);
        Glide.with(mContext).load(familyModel.getUser_image()).asBitmap().placeholder(R.mipmap.splash)
                .into(civ_image);
        Glide.with(mContext).load(familyModel.getUser_image()).asBitmap().placeholder(R.mipmap.splash)
                .into(Background);
        tvFamilyName.setText(familyModel.getUser_name());
        tvCity.setText(familyModel.getUser_city());
        rbRating.setRating(familyModel.getShop_rate());
    }

}
