package com.aait.ayadenaaclient.UI.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.ayadenaaclient.App.Constant;
import com.aait.ayadenaaclient.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaclient.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaclient.Models.ProductModel;
import com.aait.ayadenaaclient.Models.TrackModel;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.bumptech.glide.Glide;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyTracksAdapter extends ParentRecyclerAdapter<TrackModel> {
    private String errorMsg;
    public MyTracksAdapter(Context context, List<TrackModel> data) {
        super(context, data);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        switch (viewType) {
            case Constant.InfinitScroll.ITEM:
                View viewItem = inflater.inflate(R.layout.recycler_my_tracks, parent, false);
                viewHolder = new MyTracksAdapter.OldOrdersAdapter(viewItem);
                break;
            case Constant.InfinitScroll.LOADING:
                View viewLoading = inflater.inflate(R.layout.recycle_item_progress, parent, false);
                viewHolder = new MyTracksAdapter.LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case Constant.InfinitScroll.ITEM:
                final MyTracksAdapter.OldOrdersAdapter ordersAdapter = (MyTracksAdapter.OldOrdersAdapter) holder;
                TrackModel trackModel = data.get(position);
                ordersAdapter.date.setText(trackModel.getCreated_at());
                ordersAdapter.time.setText(trackModel.getBook_date());
                ordersAdapter.name.setText(mcontext.getResources().getString(R.string.name)+":"+trackModel.getProvider_name());
                ordersAdapter.phone.setText(mcontext.getResources().getString(R.string.mobile_number)+":"+trackModel.getProvider_phone());
                ordersAdapter.address.setText(mcontext.getResources().getString(R.string.address)+":"+trackModel.getAddress());
                ordersAdapter.person_num.setText(mcontext.getResources().getString(R.string.num_persons)+":"+trackModel.getPersons());
                ordersAdapter.price.setText(mcontext.getResources().getString(R.string.price)+trackModel.getPrice());
                ordersAdapter.status.setText(trackModel.getStatus()+"");
                if (trackModel.getStatus()!=5){
                    ordersAdapter.receive.setVisibility(View.GONE);
                }
                if (trackModel.getStatus()==5){
                    ordersAdapter.receive.setVisibility(View.VISIBLE);
                    ordersAdapter.receive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            itemClickListener.onItemClick(v,position);
                        }
                    });
                }


                break;

            case Constant.InfinitScroll.LOADING:
                OffersAdapter.LoadingVH loadingVH = (OffersAdapter.LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    mcontext.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == data.size() - 1 && isLoadingAdded)
                ? Constant.InfinitScroll.LOADING
                : Constant.InfinitScroll.ITEM;
    }

    protected class OldOrdersAdapter extends ParentRecyclerViewHolder {

        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.phone)
        TextView phone;
        @BindView(R.id.person_num)
        TextView person_num;
        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.receive)
        Button receive;


        public OldOrdersAdapter(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }


    protected class LoadingVH extends ParentRecyclerViewHolder {


        @BindView(R.id.progress_wheel)
        ProgressWheel mProgressBar;

        @BindView(R.id.loadmore_retry)
        ImageButton mRetryBtn;

        @BindView(R.id.loadmore_errortxt)
        TextView mErrorTxt;

        @BindView(R.id.loadmore_errorlayout)
        LinearLayout mErrorLayout;


        public LoadingVH(View itemView) {
            super(itemView);
            ProgressWheel mProgressBar = (ProgressWheel) findViewById(R.id.progress_wheel);
        }

        @OnClick(R.id.loadmore_retry)
        void onLoadMoreRetryClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }

        @OnClick(R.id.loadmore_errorlayout)
        void onLoadMoreErrorLayoutClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }
    }


    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(data.size() - 1);
        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }

}
