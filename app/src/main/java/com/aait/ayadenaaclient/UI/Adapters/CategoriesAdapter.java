package com.aait.ayadenaaclient.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaclient.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaclient.Models.CategoriesModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */

public class CategoriesAdapter extends ParentRecyclerAdapter<CategoriesModel> {

    public CategoriesAdapter(final Context context, final List<CategoriesModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        CategoriesModel categoryModel = data.get(position);
        viewHolder.tvCatName.setText(categoryModel.getname());
        Glide.with(mcontext).load(categoryModel.getImage()).asBitmap().placeholder(R.mipmap.logo)
                .into(viewHolder.ivCatImage);
    }


    public class ViewHolder extends ParentRecyclerViewHolder {


        @BindView(R.id.iv_cat_image)
        ImageView ivCatImage;

        @BindView(R.id.tv_cat_name)
        TextView tvCatName;

        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}
