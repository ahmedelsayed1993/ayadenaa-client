package com.aait.ayadenaaclient.UI.Activities;

import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.aait.ayadenaaclient.Base.ParentActivity;
import com.aait.ayadenaaclient.Models.BaseResponse;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.aait.ayadenaaclient.Uitls.PermissionUtils;
import com.aait.ayadenaaclient.Uitls.ProgressRequestBody;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.ayadenaaclient.App.Constant.RequestPermission.REQUEST_IMAGES;

public class BankTransferActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks {
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.bank_name)
    EditText bank_name;
    @BindView(R.id.account_owner_name)
    EditText account_owner_name;
    @BindView(R.id.account_number)
    EditText account_number;
    @BindView(R.id.iban_number)
    EditText iban_number;
    @BindView(R.id.amount)
    EditText amount;
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.bank_transfer));

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_transfer;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
    boolean validate(){
        if (bank_name.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.bank_name));
            return false;
        }else if (account_owner_name.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.account_owner_name));
            return false;
        }else if (account_number.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.account_number));
            return false;
        }else if (iban_number.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.iban_number));
            return false;
        }else if (amount.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.amount));
            return false;
        }else if (ImageBasePath==null){
            CommonUtil.makeToast(mContext,getString(R.string.bill_image));
            return false;
        }
        return true;
    }
    @OnClick(R.id.add_img)
    void onAddClick(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        image.setImageURI(Uri.parse(ImageBasePath));
                        image.setVisibility(View.VISIBLE);

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }
    @OnClick(R.id.send)
    void onSendClick(){
        if (validate()){
            transfer(ImageBasePath);
        }
    }
    private void transfer(String PathFromImage){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(PathFromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, BankTransferActivity.this);
        filePart = MultipartBody.Part.createFormData("image", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).transfer(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),1,bank_name.getText().toString(),account_owner_name.getText().toString(),account_number.getText().toString(),iban_number.getText().toString(),amount.getText().toString(),filePart).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        onBackPressed();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

}
