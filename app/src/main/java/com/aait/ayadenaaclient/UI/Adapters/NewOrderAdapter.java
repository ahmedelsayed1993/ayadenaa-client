package com.aait.ayadenaaclient.UI.Adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.ayadenaaclient.App.Constant;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaclient.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaclient.Models.OrderModel;
import com.bumptech.glide.Glide;
import com.pnikosis.materialishprogress.ProgressWheel;


import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class NewOrderAdapter extends ParentRecyclerAdapter<OrderModel> {

    private String errorMsg;

    public NewOrderAdapter(final Context context, final List<OrderModel> data) {
        super(context, data);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        switch (viewType) {
            case Constant.InfinitScroll.ITEM:
                View viewItem = inflater.inflate(R.layout.recycle_order_row, parent, false);
                viewHolder = new NewOrderViewHolder(viewItem);
                break;
            case Constant.InfinitScroll.LOADING:
                View viewLoading = inflater.inflate(R.layout.recycle_item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case Constant.InfinitScroll.ITEM:
                NewOrderViewHolder newOrderViewHolder = (NewOrderViewHolder) holder;
                OrderModel orderModel = data.get(position);
                Glide.with(mcontext).load(orderModel.getOrder_provider_avatar()).asBitmap()
                        .into(newOrderViewHolder.ivUserImage);
                newOrderViewHolder.user_name.setText(orderModel.getOrder_provider_name());
                if (orderModel.getOrder_status()==3){
                    newOrderViewHolder.refused.setVisibility(View.VISIBLE);
                    newOrderViewHolder.iv_step_five.setVisibility(View.GONE);
                    newOrderViewHolder.iv_order_status.setVisibility(View.GONE);
                    newOrderViewHolder.tv_step_four.setVisibility(View.GONE);
                    newOrderViewHolder.iv_step_four.setVisibility(View.GONE);
                    newOrderViewHolder.tv_step_five.setVisibility(View.GONE);
                    newOrderViewHolder.tv_step_three.setVisibility(View.GONE);
                    newOrderViewHolder.iv_step_three.setVisibility(View.GONE);
                    newOrderViewHolder.tv_step_two.setVisibility(View.GONE);
                    newOrderViewHolder.iv_step_two.setVisibility(View.GONE);
                    newOrderViewHolder.tv_step_one.setVisibility(View.GONE);
                    newOrderViewHolder.iv_step_one.setVisibility(View.GONE);

                }
                if (orderModel.getOrder_status()>=4){
                    newOrderViewHolder.iv_step_two.setImageResource(R.mipmap.step_two);
                    newOrderViewHolder.tv_step_two.setTextColor(mcontext.getResources().getColor(R.color.order_step_2_color));
                    newOrderViewHolder.iv_order_status.setImageResource(R.mipmap.order_two);
                    newOrderViewHolder.refused.setVisibility(View.GONE);
                }
                if (orderModel.getOrder_status()>=6){
                    newOrderViewHolder.iv_step_three.setImageResource(R.mipmap.step_three);
                    newOrderViewHolder.tv_step_three.setTextColor(mcontext.getResources().getColor(R.color.order_step_3_color));
                    newOrderViewHolder.iv_order_status.setImageResource(R.mipmap.order_three);
                    newOrderViewHolder.refused.setVisibility(View.GONE);
                }
                if (orderModel.getOrder_status()>=7){
                    newOrderViewHolder.iv_step_four.setImageResource(R.mipmap.step_four);
                    newOrderViewHolder.tv_step_four.setTextColor(mcontext.getResources().getColor(R.color.order_step_4_color));
                    newOrderViewHolder.iv_order_status.setImageResource(R.mipmap.order_four);
                    newOrderViewHolder.refused.setVisibility(View.GONE);
                }if (orderModel.getOrder_status()>=8){
                newOrderViewHolder.iv_step_five.setImageResource(R.mipmap.step_five);
                newOrderViewHolder.tv_step_five.setTextColor(mcontext.getResources().getColor(R.color.order_step_5_color));
                newOrderViewHolder.iv_order_status.setImageResource(R.mipmap.order_five);
                newOrderViewHolder.refused.setVisibility(View.GONE);
                }
                if (orderModel.getOrder_status()==8){
                    newOrderViewHolder.receive.setVisibility(View.VISIBLE);
                }
                newOrderViewHolder.receive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        itemClickListener.onItemClick(v,position);
                    }
                });

                break;
            case Constant.InfinitScroll.LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);
                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    mcontext.getString(R.string.error_msg_unknown));
                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == data.size() - 1 && isLoadingAdded)
                ? Constant.InfinitScroll.LOADING
                : Constant.InfinitScroll.ITEM;
    }


    /**
     * Main list's content ViewHolder
     */

    protected class NewOrderViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.civ_user)
        CircleImageView ivUserImage;

        @BindView(R.id.user_name)
        TextView user_name;

        @BindView(R.id.iv_step_one)
        ImageView iv_step_one;
        @BindView(R.id.iv_step_two)
        ImageView iv_step_two;
        @BindView(R.id.iv_step_three)
        ImageView iv_step_three;
        @BindView(R.id.iv_step_four)
        ImageView iv_step_four;
        @BindView(R.id.iv_step_five)
        ImageView iv_step_five;
        @BindView(R.id.tv_step_five)
        TextView tv_step_five;
        @BindView(R.id.tv_step_four)
        TextView tv_step_four;
        @BindView(R.id.tv_step_three)
        TextView tv_step_three;
        @BindView(R.id.tv_step_two)
        TextView tv_step_two;
        @BindView(R.id.tv_step_one)
        TextView tv_step_one;
        @BindView(R.id.iv_order_status)
        ImageView iv_order_status;
        @BindView(R.id.receive)
        Button receive;
        @BindView(R.id.refused)
        TextView refused;


        public NewOrderViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    itemClickListener.onItemClick(view, getPosition());
                }
            });
        }


    }


    protected class LoadingVH extends ParentRecyclerViewHolder {


        @BindView(R.id.progress_wheel)
        ProgressWheel mProgressBar;

        @BindView(R.id.loadmore_retry)
        ImageButton mRetryBtn;

        @BindView(R.id.loadmore_errortxt)
        TextView mErrorTxt;

        @BindView(R.id.loadmore_errorlayout)
        LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            ProgressWheel mProgressBar = (ProgressWheel) findViewById(R.id.progress_wheel);
        }

        @OnClick(R.id.loadmore_retry)
        void onLoadMoreRetryClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }

        @OnClick(R.id.loadmore_errorlayout)
        void onLoadMoreErrorLayoutClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }
    }


    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(data.size() - 1);
        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }


}

