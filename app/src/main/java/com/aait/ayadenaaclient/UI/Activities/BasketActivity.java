package com.aait.ayadenaaclient.UI.Activities;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.ayadenaaclient.Base.ParentActivity;
import com.aait.ayadenaaclient.Listener.OnItemClickListener;

import com.aait.ayadenaaclient.Listener.OrderHasDelivery;
import com.aait.ayadenaaclient.Models.BaseResponse;
import com.aait.ayadenaaclient.Models.BasketResponse;
import com.aait.ayadenaaclient.Models.CartProducts;
import com.aait.ayadenaaclient.Models.CartResponse;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.UI.Adapters.CardBasketAdapter;

import com.aait.ayadenaaclient.UI.Fragments.PayTypeFragment;
import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BasketActivity extends ParentActivity implements OnItemClickListener,OrderHasDelivery{

    @BindView(R.id.basket_recycler)
            RecyclerView basket_recycler;

    LinearLayoutManager linearLayoutManager;
    ArrayList<CartResponse> cartProducts = new ArrayList<>();
    CardBasketAdapter cardBasketAdapter;
    @BindView(R.id.btn_confirm_order)
    Button btn_confirm_order;

    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.basket));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        cardBasketAdapter = new CardBasketAdapter(mContext,cartProducts,R.layout.recycle_card_basket);
        basket_recycler.setLayoutManager(linearLayoutManager);
        basket_recycler.setAdapter(cardBasketAdapter);


            MyCart(mSharedPrefManager.getUserData().getUser_id());

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_card_basket;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {


    }



    private void MyCart(int user_id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).myCart(mLanguagePrefManager.getAppLanguage(),user_id).enqueue(new Callback<BasketResponse>() {
            @Override
            public void onResponse(Call<BasketResponse> call, final Response<BasketResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        Log.e("xxx",new Gson().toJson(response.body().getData()));
                        if (response.body().getData().size()==0){
                            btn_confirm_order.setVisibility(View.GONE);

                            CommonUtil.makeToast(mContext,getString(R.string.no_data));
                        }else {
                            cardBasketAdapter.updateAll(response.body().getData());

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BasketResponse> call, Throwable t) {
                Log.e("vvv",new Gson().toJson(t));
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.btn_confirm_order)
    void onConfirm(){
        showEditProduct(getSupportFragmentManager());
    }
    private void doOrder(int way_type){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).doOrder(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),way_type).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        startActivity(new Intent(mContext,MainActivity.class));

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    void showEditProduct(FragmentManager mFragmentManager) {
        PayTypeFragment payTypeFragment = PayTypeFragment.newInstance();
        payTypeFragment.show(mFragmentManager, "Choose Deivery");
        payTypeFragment.setListner(this);
    }

    @Override
    public void orderHasDelivery(int hasDelivery) {
        if (hasDelivery==1){
            doOrder(1);
        }else if (hasDelivery==2){
            doOrder(2);

        }

    }
}
