package com.aait.ayadenaaclient.UI.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.ayadenaaclient.App.Constant;
import com.aait.ayadenaaclient.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaclient.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaclient.Models.OrderModel;
import com.aait.ayadenaaclient.Models.TracksModel;
import com.aait.ayadenaaclient.R;
import com.bumptech.glide.Glide;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class TracksAdapter extends ParentRecyclerAdapter<TracksModel> {
    private String errorMsg;

    public TracksAdapter(Context context, List<TracksModel> data) {
        super(context, data);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        switch (viewType) {
            case Constant.InfinitScroll.ITEM:
                View viewItem = inflater.inflate(R.layout.recycler_track, parent, false);
                viewHolder = new TracksAdapter.OldOrdersAdapter(viewItem);
                break;
            case Constant.InfinitScroll.LOADING:
                View viewLoading = inflater.inflate(R.layout.recycle_item_progress, parent, false);
                viewHolder = new TracksAdapter.LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case Constant.InfinitScroll.ITEM:
                TracksAdapter.OldOrdersAdapter newOrderViewHolder = (TracksAdapter.OldOrdersAdapter) holder;
                TracksModel orderModel = data.get(position);
                Glide.with(mcontext).load(orderModel.getShop_user_image()).asBitmap().placeholder(R.mipmap.splash)
                        .into(newOrderViewHolder.track_image);
                newOrderViewHolder.name.setText(orderModel.getShop_user_name());

                newOrderViewHolder.city.setText(mcontext.getResources().getString(R.string.city)+": "+orderModel.getShop_city() );
                newOrderViewHolder.person_number.setText(mcontext.getResources().getString(R.string.person_value)+":"+orderModel.getShop_minimum_person());
               newOrderViewHolder.person_price.setText(mcontext.getResources().getString(R.string.person_price)+":"+orderModel.getShop_person_price());


                newOrderViewHolder.book_track.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        itemClickListener.onItemClick(view, position);
                    }
                });
                if (orderModel.getShop_categories() != null) {
                    if (orderModel.getShop_categories().size() >= 3) {
                        newOrderViewHolder.tvCategoryOne
                                .setText(orderModel.getShop_categories().get(0).getName());
                        newOrderViewHolder.tvCategoryTwo
                                .setText(orderModel.getShop_categories().get(1).getName());
                        newOrderViewHolder.tvCategoryThree
                                .setText(orderModel.getShop_categories().get(2).getName());
                    } else if (orderModel.getShop_categories().size() == 2) {
                        newOrderViewHolder.tvCategoryOne
                                .setText(orderModel.getShop_categories().get(0).getName());
                        newOrderViewHolder.tvCategoryTwo
                                .setText(orderModel.getShop_categories().get(1).getName());
                        newOrderViewHolder.tvCategoryThree.setVisibility(View.GONE);
                    } else if (orderModel.getShop_categories().size() == 1) {
                        newOrderViewHolder.tvCategoryOne
                                .setText(orderModel.getShop_categories().get(0).getName());
                        newOrderViewHolder.tvCategoryTwo.setVisibility(View.GONE);
                        newOrderViewHolder.tvCategoryThree.setVisibility(View.GONE);
                    } else if (orderModel.getShop_categories().size() == 0) {
                        newOrderViewHolder.tvCategoryOne.setVisibility(View.GONE);
                        newOrderViewHolder.tvCategoryTwo.setVisibility(View.GONE);
                        newOrderViewHolder.tvCategoryThree.setVisibility(View.GONE);
                    }
                }
                break;

            case Constant.InfinitScroll.LOADING:
                TracksAdapter.LoadingVH loadingVH = (TracksAdapter.LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    mcontext.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }
    protected class OldOrdersAdapter extends ParentRecyclerViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.person_number)
        TextView person_number;
        @BindView(R.id.person_price)
        TextView person_price;
        @BindView(R.id.track_image)
        CircleImageView track_image;
        @BindView(R.id.city)
        TextView city;
        @BindView(R.id.book_track)
        Button book_track;
        @BindView(R.id.tv_category_one)
        TextView tvCategoryOne;

        @BindView(R.id.tv_category_two)
        TextView tvCategoryTwo;

        @BindView(R.id.tv_category_three)
        TextView tvCategoryThree;


        public OldOrdersAdapter(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }


    protected class LoadingVH extends ParentRecyclerViewHolder {


        @BindView(R.id.progress_wheel)
        ProgressWheel mProgressBar;

        @BindView(R.id.loadmore_retry)
        ImageButton mRetryBtn;

        @BindView(R.id.loadmore_errortxt)
        TextView mErrorTxt;

        @BindView(R.id.loadmore_errorlayout)
        LinearLayout mErrorLayout;


        public LoadingVH(View itemView) {
            super(itemView);
            ProgressWheel mProgressBar = (ProgressWheel) findViewById(R.id.progress_wheel);
        }

        @OnClick(R.id.loadmore_retry)
        void onLoadMoreRetryClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }

        @OnClick(R.id.loadmore_errorlayout)
        void onLoadMoreErrorLayoutClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }
    }


    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(data.size() - 1);
        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }


}
