package com.aait.ayadenaaclient.UI.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.ayadenaaclient.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaclient.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaclient.Models.AdditionsModel;
import com.aait.ayadenaaclient.Models.ListModel;
import com.aait.ayadenaaclient.R;

import java.util.List;

import butterknife.BindView;

public class TrackCategoriesAdapter extends ParentRecyclerAdapter<ListModel> {
    public TrackCategoriesAdapter(Context context, List<ListModel> data) {
        super(context, data);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        View viewItem = inflater.inflate(R.layout.recycler_track_categories, parent, false);
        viewHolder = new TrackCategoriesAdapter.ViewHolder(viewItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        final TrackCategoriesAdapter.ViewHolder viewHolder = (TrackCategoriesAdapter.ViewHolder) holder;
        ListModel listModel = data.get(position);
        viewHolder.cat.setText(listModel.getName());
        viewHolder.cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v,position);
            }
        });
        if (listModel.isChecked()) {

            viewHolder.cat.setChecked(true);
        } else {
           viewHolder.cat.setChecked(false);
        }
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

       @BindView(R.id.cat)
       CheckBox cat;


        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}
