package com.aait.ayadenaaclient.UI.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.ayadenaaclient.Models.CartProducts;
import com.aait.ayadenaaclient.Models.CartResponse;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;

import com.aait.ayadenaaclient.UI.Activities.EditOrderItemActivity;
import com.aait.ayadenaaclient.UI.Activities.MainActivity;
import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.aait.ayadenaaclient.Uitls.DialogUtil;
import com.aait.ayadenaaclient.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaclient.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaclient.Listener.OnItemClickListener;
import com.aait.ayadenaaclient.Models.BaseResponse;

import com.bumptech.glide.Glide;

import java.text.MessageFormat;
import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CardBasketAdapter extends ParentRecyclerAdapter<CartResponse> {

    public CardBasketAdapter(final Context context, final List<CartResponse> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        final CartResponse basketModel = data.get(position);

        Glide.with(mcontext).load(basketModel.getProvider_image()).asBitmap().placeholder(R.mipmap.splash)
                .into(viewHolder.civFamilyImage);
        viewHolder.tvFamilyName.setText(basketModel.getProvider_name());

        viewHolder.tvDeleveryCost
                .setText(basketModel.getDelegate_price()+
                        mcontext.getResources().getString(R.string.SAR));
        viewHolder.tvTotalCoast
                .setText(( basketModel.getTotal_price()+basketModel.getDelegate_price())+
                        mcontext.getResources().getString(R.string.SAR));
        viewHolder.familyOrderDetailsAdapter.setData(basketModel.getCart_products());
        viewHolder.familyOrderDetailsAdapter.setRowIndex(position);
        viewHolder.familyOrderDetailsAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(final View view, final int position) {
                if (view.getId()==R.id.iv_edit){
                    Log.e("ffff","nnnn");
                    EditOrderItemActivity.startActivity((AppCompatActivity)mcontext,basketModel.getCart_products().get(position));
                }else if (view.getId() == R.id.iv_delete){
                    DialogUtil.showAlertDialog(mcontext, mcontext.getString(R.string.delete_order_desc),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(final DialogInterface dialogInterface, final int i) {
                                    delete(basketModel.getCart_products().get(position).getCart_product_id());


                                }
                            });
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends ParentRecyclerViewHolder {
        private FamilyOrderDetailsAdapter familyOrderDetailsAdapter;
        @BindView(R.id.civ_family_image)
        CircleImageView civFamilyImage;

        @BindView(R.id.tv_family_name)
        TextView tvFamilyName;



        @BindView(R.id.tv_delevery_cost)
        TextView tvDeleveryCost;

        @BindView(R.id.tv_total_coast)
        TextView tvTotalCoast;
        @BindView(R.id.product_recycler)
        RecyclerView recyclerProducts;


        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
            recyclerProducts.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL,false));
            familyOrderDetailsAdapter = new  FamilyOrderDetailsAdapter(mcontext);
            recyclerProducts.setAdapter(familyOrderDetailsAdapter);
        }


    }
    private void delete(int product){

        RetroWeb.getClient().create(ServiceApi.class).deleteCart(mSharedPrefManager.getUserData().getUser_id(),product).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mcontext,response.body().getMsg());
                        MainActivity.startActivity((AppCompatActivity)mcontext);
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

                CommonUtil.handleException(mcontext,t);
                t.printStackTrace();
            }
        });
    }
}

