package com.aait.ayadenaaclient.UI.Adapters;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.ayadenaaclient.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaclient.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaclient.Models.CartProductsModel;
import com.aait.ayadenaaclient.R;


import java.util.List;

import butterknife.BindView;

public class FamilyOrderDetailsAdapter extends ParentRecyclerAdapter<CartProductsModel> {
    private int mRowIndex = -1;

    public FamilyOrderDetailsAdapter(final Context context) {
        super(context);
    }
    public void setRowIndex(int index) {
        mRowIndex = index;
    }
    public void setData(List<CartProductsModel> products) {
        if (data != products) {
            data = products;
            notifyDataSetChanged();
        }
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(R.layout.recycler_food_family, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        final CartProductsModel foodModel = data.get(position);


        viewHolder.tvItemPrice.setText(foodModel.getTotal_product_price() + " "+mcontext.getResources().getString(R.string.SAR));
        viewHolder.tvItemName.setText(foodModel.getProduct_name());

        // check if the food has discount or not .





        viewHolder.iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v,position);
             //  EditOrderItemActivity.startActivity((AppCompatActivity)mcontext,data.get(position));

            }
        });
        viewHolder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
         itemClickListener.onItemClick(v,position);

            }
        });
        viewHolder.mFamilyOrderSpecialAdditiveAdapter.setData(data.get(position).getCart_additions()); // List of Strings
        viewHolder.mFamilyOrderSpecialAdditiveAdapter.setRowIndex(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        private FamilyOrderSpecialAdditiveAdapter mFamilyOrderSpecialAdditiveAdapter;



        @BindView(R.id.tv_item_price)
        TextView tvItemPrice;

        @BindView(R.id.tv_item_name)
        TextView tvItemName;



        @BindView(R.id.tv_recycle)
        RecyclerView tvRecycle;
        @BindView(R.id.iv_edit)
        ImageView iv_edit;
        @BindView(R.id.iv_delete)
        ImageView iv_delete;








        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);

            tvRecycle.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false));
            mFamilyOrderSpecialAdditiveAdapter = new FamilyOrderSpecialAdditiveAdapter(mcontext);
            tvRecycle.setAdapter(mFamilyOrderSpecialAdditiveAdapter);
        }

    }


}
