package com.aait.ayadenaaclient.UI.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;

import com.aait.ayadenaaclient.Listener.OrderHasDelivery;
import com.aait.ayadenaaclient.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class PayTypeFragment extends DialogFragment {
    OrderHasDelivery listner;
    public PayTypeFragment(){

    }
    boolean ischeched = true;
    @BindView(R.id.wallet)
    CheckBox wallet;
    @BindView(R.id.cash)
    CheckBox cash;
    public void setListner(OrderHasDelivery listner) {
        this.listner = listner;
    }
    public static PayTypeFragment newInstance() {
        PayTypeFragment frag = new PayTypeFragment();
        return frag;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_pay_type, container);
        ButterKnife.bind(this, view);

        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }
    @OnCheckedChanged(R.id.cash)
    void onCash(){
        if (cash.isChecked()) {
            wallet.setChecked(false);
        }
    }
    @OnCheckedChanged(R.id.wallet)
    void onWallet(){
        if (wallet.isChecked()) {
            cash.setChecked(false);
        }
    }
    @Override
    public void onResume() {
        // Get existing layout params for the window
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        // Assign window properties to fill the parent
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);
        getDialog().getWindow().setWindowAnimations(R.style.DialogAnimation_2);
        // Call super onResume after sizing
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
    @OnClick(R.id.accept)
    void onAcceptClick(){
        if (cash.isChecked()){
           if (wallet.isChecked()){
               wallet.setChecked(false);
           }
            listner.orderHasDelivery(1);

        }else if (wallet.isChecked()){
            if (cash.isChecked()){
                cash.setChecked(false);
            }

            listner.orderHasDelivery(2);
        }
        dismiss();
    }

}
