package com.aait.ayadenaaclient.UI.Activities;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.ayadenaaclient.Models.UserModel;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;


import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.aait.ayadenaaclient.Uitls.ValidationUtils;
import com.aait.ayadenaaclient.Base.ParentActivity;
import com.aait.ayadenaaclient.Fcm.MyFirebaseInstanceIDService;
import com.aait.ayadenaaclient.Models.LoginResponse;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends ParentActivity {

    @BindView(R.id.lay_splash)
    LinearLayout laySplash;

    @BindView(R.id.til_name)
    TextInputLayout tilName;

    @BindView(R.id.et_name)
    TextInputEditText etName;

    @BindView(R.id.til_password)
    TextInputLayout tilPassword;

    @BindView(R.id.et_password)
    TextInputEditText etPassword;

    @BindView(R.id.tv_forget_pass)
    TextView tvForgetPass;
    String type;
    UserModel userModel;

    public static void startActivity(AppCompatActivity mAppCompatActivity,String type) {
        Intent mIntent = new Intent(mAppCompatActivity, LoginActivity.class);
        mIntent.putExtra("type",type);
        mIntent.addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    void getBundleData(){
        type = (String)getIntent().getSerializableExtra("type");
    }

    @Override
    protected void initializeComponents() {
        getBundleData();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.tv_forget_pass)
    void onForgetPAssClick() {
        ForgetPasswordActivity.startActivity((AppCompatActivity) mContext,type);
    }
    @OnClick(R.id.btn_new_account)
    void onBtnNewAccountClick() {
       startActivity(new Intent(mContext,RegisterActivity.class));
    }
    @OnClick(R.id.btn_login)
    void onBtnLoginClick() {
        if (loginValidation()) {
            Login();
        }
    }
    @OnClick(R.id.btn_visitor)
    void onBrowseClick(){
        mSharedPrefManager.setLoginStatus(false);
        MainActivity.startActivity((AppCompatActivity)mContext);
    }

    boolean loginValidation() {
        if (!ValidationUtils.checkError(etName, tilName, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etPassword, tilName, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }
    private void Login(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).Login(mLanguagePrefManager.getAppLanguage(),
                etName.getText().toString(),etPassword.getText().toString(),"else", MyFirebaseInstanceIDService.getToken(mContext)).
                enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    Log.e("kkkkk",new Gson().toJson(response.body()));

                        if (response.body().getStatus()== 1){

                            mSharedPrefManager.setUserData(response.body().getData());
                            mSharedPrefManager.setLoginStatus(true);
                            MainActivity.startActivity((AppCompatActivity)mContext);
                        }else if (response.body().getStatus() == 2){
                            ConfirmCodeActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getUser_id()+"");
                        }
                        else if (response.body().getStatus() == 0)
                        {    emptyForm();
                            CommonUtil.makeToast(mContext,response.body().getMsg());
                        }
                    }
                    else {
                        emptyForm();
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Log.e("error",response.body().getMsg());
                    }
                }


            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("jjjjjj",new Gson().toJson(t));
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }

    void emptyForm() {
        etName.setText("");
        etPassword.setText("");
    }
}
