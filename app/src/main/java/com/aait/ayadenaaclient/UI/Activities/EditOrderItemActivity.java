package com.aait.ayadenaaclient.UI.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.ayadenaaclient.App.Constant.BundleData;
import com.aait.ayadenaaclient.Base.ParentActivity;
import com.aait.ayadenaaclient.Listener.OnItemClickListener;
import com.aait.ayadenaaclient.Models.AddtionModel;
import com.aait.ayadenaaclient.Models.BaseResponse;
import com.aait.ayadenaaclient.Models.CartAdditions;
import com.aait.ayadenaaclient.Models.CartProductsModel;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.UI.Adapters.SpecialAdditiveAdapter1;
import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahmoud on 2/17/18.
 */

public class EditOrderItemActivity extends ParentActivity implements OnItemClickListener {


    @BindView(R.id.iv_image)
    ImageView ivImage;

    @BindView(R.id.tv_food_name)
    TextView tvFoodName;

    @BindView(R.id.tv_normal_price)
    TextView tvNormalPrice;





    @BindView(R.id.ed_order_number)
    EditText edOrderNumber;

    @BindView(R.id.rv_Recycle)
    RecyclerView rvRecycle;

    LinearLayoutManager linearLayoutManager;

    SpecialAdditiveAdapter1 mSpecialAdditiveAdapter;

    List<CartAdditions> mSpecialAdditiveModels = new ArrayList<>();
    List<AddtionModel> addtionModels = new ArrayList<>();
    AddtionModel addtionModel;


    CartProductsModel mFoodModel;

    public static void startActivity(AppCompatActivity mAppCompatActivity, CartProductsModel foodModel) {
        Intent mIntent = new Intent(mAppCompatActivity, EditOrderItemActivity.class);
        mIntent.putExtra(BundleData.FOOD_MODEL, foodModel);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData() {
        mFoodModel = (CartProductsModel) getIntent().getSerializableExtra(BundleData.FOOD_MODEL);
        for (int i = 0;i<mFoodModel.getCart_additions().size();i++){
            addtionModels.add(new AddtionModel(mFoodModel.getCart_additions().get(i).getAddition_id(),mFoodModel.getCart_additions().get(i).getAddition_count()));

        }
        mSpecialAdditiveModels = mFoodModel.getCart_additions();
    }

    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.basket));
        getBundleData();
        setData();

        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mSpecialAdditiveAdapter = new SpecialAdditiveAdapter1(mContext, mSpecialAdditiveModels,
                R.layout.recycle_special_adictive);
        mSpecialAdditiveAdapter.setOnItemClickListener(this);
        rvRecycle.setAdapter(mSpecialAdditiveAdapter);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_edit_order_item1;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @OnClick(R.id.iv_minus)
    void onMinusClick() {
        if (Integer.parseInt(edOrderNumber.getText().toString()) == 1) {

        } else {
            edOrderNumber.setText((Integer.parseInt(edOrderNumber.getText().toString()) - 1) + "");
            float price = mFoodModel.getTotal_product_price();
            String price1 =price*Integer.parseInt(edOrderNumber.getText().toString())+"";
            tvNormalPrice.setText(price1+ " " + getResources().getString(R.string.SAR));
        }
    }

    @OnClick(R.id.iv_add)
    void onAddClick() {
        edOrderNumber.setText((Integer.parseInt(edOrderNumber.getText().toString()) + 1) + "");
        float price = mFoodModel.getTotal_product_price();
        String price1 =price*Integer.parseInt(edOrderNumber.getText().toString())+"";
        tvNormalPrice.setText(price1+ " " + getResources().getString(R.string.SAR));
    }

    @OnClick(R.id.btn_add_to_card)
    void onBtnAddToCardClick() {
        CommonUtil.PrintLogE(new Gson().toJson(addtionModels));
        editOrder(
                Integer.parseInt(edOrderNumber.getText().toString()), new Gson().toJson(addtionModels)
                );
    }

    @Override
    public void onItemClick(final View view, final int position) {
        mSpecialAdditiveModels.get(position);
        if (view.getId() == R.id.iv_minus) {
            if (mSpecialAdditiveModels.get(position).getAddition_count() == 0) {

            } else {
                mSpecialAdditiveModels.get(position).setAddition_count(mSpecialAdditiveModels.get(position).getAddition_count() - 1);
                addtionModels.get(position).setCount(addtionModels.get(position).getCount()-1);
                mSpecialAdditiveAdapter.notifyDataSetChanged();
            }
        } else if (view.getId() == R.id.iv_add) {
            mSpecialAdditiveModels.get(position).setAddition_count(mSpecialAdditiveModels.get(position).getAddition_count() + 1);
            addtionModels.get(position).setCount(addtionModels.get(position).getCount()+1);
            mSpecialAdditiveAdapter.notifyDataSetChanged();
        }
    }

    void setData() {
        Glide.with(mContext).load(mFoodModel.getProduct_image()).asBitmap().placeholder(R.mipmap.splash)
                .into(ivImage);
        tvFoodName.setText(mFoodModel.getProduct_name());
        tvNormalPrice.setText(mFoodModel.getTotal_product_price() + " " + mContext.getResources().getString(R.string.SAR));


        edOrderNumber.setText(mFoodModel.getProduct_count()+"");
    }


    private void editOrder( int count, String additions) {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).updateCart(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getUser_id(), mFoodModel.getCart_product_id(), count, additions)
                .enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call,
                            Response<BaseResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()==1) {
                                CommonUtil.makeToast(mContext, response.body().getMsg());
                                startActivity(new Intent(mContext,BasketActivity.class));
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        hideProgressDialog();
                    }
                });
    }
}
