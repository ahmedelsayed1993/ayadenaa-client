package com.aait.ayadenaaclient.UI.Adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.ayadenaaclient.App.Constant;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaclient.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaclient.Models.OrderModel;
import com.bumptech.glide.Glide;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class OldOrderAdapter extends ParentRecyclerAdapter<OrderModel> {

    private String errorMsg;

    public OldOrderAdapter(final Context context, final List<OrderModel> data) {
        super(context, data);
    }


    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        switch (viewType) {
            case Constant.InfinitScroll.ITEM:
                View viewItem = inflater.inflate(R.layout.recycler_previousorders, parent, false);
                viewHolder = new OldOrdersAdapter(viewItem);
                break;
            case Constant.InfinitScroll.LOADING:
                View viewLoading = inflater.inflate(R.layout.recycle_item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case Constant.InfinitScroll.ITEM:
                OldOrdersAdapter newOrderViewHolder = (OldOrdersAdapter) holder;
                OrderModel orderModel = data.get(position);
                Glide.with(mcontext).load(orderModel.getOrder_provider_avatar()).asBitmap().placeholder(R.mipmap.splash)
                        .into(newOrderViewHolder.civ_user);
                newOrderViewHolder.user_name.setText(orderModel.getOrder_provider_name());

                newOrderViewHolder.order_number.setText(mcontext.getResources().getString(R.string.order_number)+" "+orderModel.getOrder_id() + "");
                newOrderViewHolder.delivery.setText(mcontext.getResources().getString(R.string.deliver)+":"+orderModel.getOrder_delegate_name());
                if (orderModel.getOrder_way_type()==1){
                    newOrderViewHolder.pay_type.setText(mcontext.getResources().getString(R.string.at_receive));
                }else if (orderModel.getOrder_way_type()!=1){
                    newOrderViewHolder.pay_type.setText(mcontext.getResources().getString(R.string.from_wallet));
                }


                newOrderViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        itemClickListener.onItemClick(view, position);
                    }
                });
                break;

            case Constant.InfinitScroll.LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    mcontext.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == data.size() - 1 && isLoadingAdded)
                ? Constant.InfinitScroll.LOADING
                : Constant.InfinitScroll.ITEM;
    }


    /**
     * Main list's content ViewHolder
     */

    protected class OldOrdersAdapter extends ParentRecyclerViewHolder {

      @BindView(R.id.pay_type)
      TextView pay_type;
      @BindView(R.id.delivery)
      TextView delivery;
      @BindView(R.id.user_name)
      TextView user_name;
      @BindView(R.id.civ_user)
      CircleImageView civ_user;
      @BindView(R.id.order_number)
      TextView order_number;


        public OldOrdersAdapter(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }


    protected class LoadingVH extends ParentRecyclerViewHolder {


        @BindView(R.id.progress_wheel)
        ProgressWheel mProgressBar;

        @BindView(R.id.loadmore_retry)
        ImageButton mRetryBtn;

        @BindView(R.id.loadmore_errortxt)
        TextView mErrorTxt;

        @BindView(R.id.loadmore_errorlayout)
        LinearLayout mErrorLayout;


        public LoadingVH(View itemView) {
            super(itemView);
            ProgressWheel mProgressBar = (ProgressWheel) findViewById(R.id.progress_wheel);
        }

        @OnClick(R.id.loadmore_retry)
        void onLoadMoreRetryClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }

        @OnClick(R.id.loadmore_errorlayout)
        void onLoadMoreErrorLayoutClick() {
            showRetry(false, null);
            mPaginationAdapterCallback.retryPageLoad();
        }
    }


    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(data.size() - 1);
        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }


}
