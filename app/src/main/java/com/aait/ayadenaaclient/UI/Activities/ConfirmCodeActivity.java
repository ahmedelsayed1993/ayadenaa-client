package com.aait.ayadenaaclient.UI.Activities;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;

import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.UI.Activities.LoginActivity;
import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.aait.ayadenaaclient.Uitls.ValidationUtils;
import com.aait.ayadenaaclient.Base.ParentActivity;
import com.aait.ayadenaaclient.Fcm.MyFirebaseInstanceIDService;
import com.aait.ayadenaaclient.Models.ConfirmCodeModel;


import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmCodeActivity extends ParentActivity {

    @BindView(R.id.til_code)
    TextInputLayout tilCode;

    @BindView(R.id.et_code)
    TextInputEditText etCode;


    String forgetPassModel;
    String reg ;
    String type;

String user_id;
    public static void startActivity(AppCompatActivity mAppCompatActivity,String user_id ){
        Intent mIntent = new Intent(mAppCompatActivity, ConfirmCodeActivity.class);
        mIntent.putExtra("user_id",user_id);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }



    void getBundleData() {
        user_id = (String) getIntent().getSerializableExtra("user_id");

    }
    @Override
    protected void initializeComponents() {
        getBundleData();
        reg = MyFirebaseInstanceIDService.getToken(mContext);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_confirm_account;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    boolean confirmCodeValidation() {
        if (!ValidationUtils.checkError(etCode, tilCode, getString(R.string.fill_empty))) {
            return false;
        }
        return true;
    }

    @OnClick(R.id.btn_register)
    void onBtnRegisterClick() {
        if (confirmCodeValidation()){

            confirm();

        }
    }
    private void confirm(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).confirm(user_id,etCode.getText().toString()).enqueue(new Callback<ConfirmCodeModel>() {
            @Override
            public void onResponse(Call<ConfirmCodeModel> call, Response<ConfirmCodeModel> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().issucessfull()){
                        CommonUtil.makeToast(mContext,response.body().getMsg());

                        startActivity(new Intent(mContext,LoginActivity.class));

                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ConfirmCodeModel> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }

}
