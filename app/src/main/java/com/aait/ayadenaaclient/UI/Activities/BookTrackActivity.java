package com.aait.ayadenaaclient.UI.Activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.aait.ayadenaaclient.App.Constant;
import com.aait.ayadenaaclient.Base.ParentActivity;
import com.aait.ayadenaaclient.Listener.OnItemClickListener;
import com.aait.ayadenaaclient.Listener.OrderHasDelivery;
import com.aait.ayadenaaclient.Models.BaseResponse;
import com.aait.ayadenaaclient.Models.ListModel;
import com.aait.ayadenaaclient.Models.TracksModel;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.UI.Adapters.TrackCategoriesAdapter;
import com.aait.ayadenaaclient.UI.Fragments.PayTypeFragment;
import com.aait.ayadenaaclient.Uitls.CommonUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookTrackActivity extends ParentActivity implements OnItemClickListener,OrderHasDelivery {
    final Calendar myCalendar = Calendar.getInstance();

    @BindView(R.id.book_date)
    TextView book_date;
    @BindView(R.id.book_time)
    TextView book_time;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.categories)
    RecyclerView categories;
    @BindView(R.id.notes)
    EditText notes;
    @BindView(R.id.count)
    TextView count;
    @BindView(R.id.plus)
    Button plus;
    @BindView(R.id.minus)
    Button minus;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.persons)
    TextView persons;
    @BindView(R.id.book_now)
    Button book_now;
    String category;
    TracksModel tracksModel;
    int selectedPosition;
    String mAdresse, mLang, mLat = null;
    TrackCategoriesAdapter trackCategoriesAdapter;
    LinearLayoutManager linearLayoutManager;
    DatePickerDialog.OnDateSetListener date;
    private Calendar cal;
    private int day;
    private int month;
    private int year;

    static final int DATE_PICKER_ID = 1111;
    ArrayList<ListModel> listModels = new ArrayList<>();
        public static void startActivity(AppCompatActivity mAppCompatActivity, TracksModel familyModel) {
        Intent mIntent = new Intent(mAppCompatActivity, BookTrackActivity.class);
        mIntent.putExtra(Constant.BundleData.FAMILY_MODEL, familyModel);
        mAppCompatActivity.startActivity(mIntent);
    }
    void getBundleData() {
        tracksModel = (TracksModel) getIntent().getSerializableExtra(Constant.BundleData.FAMILY_MODEL);
        listModels = tracksModel.getShop_categories();
        listModels.get(0).setChecked(true);
        category = listModels.get(0).getId()+"";
        Log.e("cat",category);

    }
    @Override
    protected void initializeComponents() {
            setToolbarTitle(getString(R.string.book_track));
        getBundleData();
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        trackCategoriesAdapter = new TrackCategoriesAdapter(mContext,listModels);
        trackCategoriesAdapter.setOnItemClickListener(this);
        categories.setLayoutManager(linearLayoutManager);
        categories.setAdapter(trackCategoriesAdapter);

        count.setText(tracksModel.getShop_minimum_person());
        price.setText(mContext.getResources().getString(R.string.price)+"("+(Integer.parseInt(count.getText().toString())*Integer.parseInt(tracksModel.getShop_person_price()))+")"+mContext.getResources().getString(R.string.SAR)+"  ");
        persons.setText(mContext.getResources().getString(R.string.fo)+count.getText().toString()+mContext.getResources().getString(R.string.person));
//         date = new DatePickerDialog.OnDateSetListener() {
//
//            @Override
//            public void onDateSet(DatePicker view, int year, int monthOfYear,
//                                  int dayOfMonth) {
//                // TODO Auto-generated method stub
//                myCalendar.set(Calendar.YEAR, year);
//                myCalendar.set(Calendar.MONTH, monthOfYear);
//                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//
//                updateLabel();
//            }
//
//        };
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:
                // create a new DatePickerDialog with values you want to show

                DatePickerDialog datePickerDialog = new DatePickerDialog(this, datePickerListener, year, month, day);
                Calendar calendar = Calendar.getInstance();

                calendar.add(Calendar.DATE, 0); // Add 0 days to Calendar
                Date newDate = calendar.getTime();
                datePickerDialog.getDatePicker().setMinDate(newDate.getTime()-(newDate.getTime()%(24*60*60*1000)));
                return datePickerDialog;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        // the callback received when the user "sets" the Date in the
        // DatePickerDialog
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {

            book_date.setText(selectedDay + "/" + (selectedMonth + 1) + "/" + selectedYear);
        }
    };

//    private void updateLabel() {
//        String myFormat = "MM/dd/yy"; //In which you need put here
//        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
////        new SimpleDateFormat("YYYY-MM-DD").parse().before(new Date());
//
//        book_date.setText(sdf.format(myCalendar.getTime()));
//    }
    @OnClick(R.id.book_date)
    void onDate(){
        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        showDialog(DATE_PICKER_ID);

    }
    @OnClick(R.id.book_time)
    void onTime(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(BookTrackActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                book_time.setText( selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }
    @OnClick(R.id.address)
    void onAddressClick(){
            MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }

    @OnClick(R.id.plus)
    void onPlus(){
            count.setText((Integer.parseInt(count.getText().toString())+1)+"");
        price.setText(mContext.getResources().getString(R.string.price)+"("+(Integer.parseInt(count.getText().toString())*Integer.parseInt(tracksModel.getShop_person_price()))+")"+mContext.getResources().getString(R.string.SAR)+"  ");
        persons.setText(mContext.getResources().getString(R.string.fo)+count.getText().toString()+mContext.getResources().getString(R.string.person));
    }
    @OnClick(R.id.minus)
    void onMinus(){
            if (count.getText().toString().equals(tracksModel.getShop_person_price())){

            }else {
                count.setText((Integer.parseInt(count.getText().toString())-1)+"");
                price.setText(mContext.getResources().getString(R.string.price)+"("+(Integer.parseInt(count.getText().toString())*Integer.parseInt(tracksModel.getShop_person_price()))+")"+mContext.getResources().getString(R.string.SAR)+"  ");
                persons.setText(mContext.getResources().getString(R.string.fo)+count.getText().toString()+mContext.getResources().getString(R.string.person));
            }
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_book_track;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
            if (view.getId() == R.id.cat){
                selectedPosition = position;
                if (!listModels.get(selectedPosition).isChecked()) {
                    for (int i = 0; i < listModels.size(); i++) {
                        listModels.get(i).setChecked(false);
                    }

                    listModels.get(selectedPosition).setChecked(true);
                    category = listModels.get(selectedPosition).getId() + "";
                    Log.e("cat", category);
                }
            }

    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse);
                    address.setText(mAdresse);
                }
            }
        }
    }
    void showEditProduct(FragmentManager mFragmentManager) {
        PayTypeFragment payTypeFragment = PayTypeFragment.newInstance();
        payTypeFragment.show(mFragmentManager, "Choose Deivery");
        payTypeFragment.setListner(this);
    }
    @OnClick(R.id.book_now)
    void onBook(){
            if (validate()) {
                showEditProduct(getSupportFragmentManager());
            }
    }

    @Override
    public void orderHasDelivery(int hasDelivery) {

                if (hasDelivery==1){
                    book("1");
                }else if (hasDelivery==2){
                    book("2");

                }


    }
    private void book(String type){
            showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).bookTrack(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getUser_id(),book_date.getText().toString(),book_time.getText().toString(),mLat,mLang,mAdresse,name.getText().toString(),phone.getText().toString(),category,count.getText().toString(),tracksModel.getShop_id()+"",type).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext,MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    boolean validate(){
            if (book_date.getText().toString().equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.book_date));
                return false;
            }else if (book_time.getText().toString().equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.book_time));
                return false;
            }else if (address.getText().toString().equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.location));
                return false;
            }else if (name.getText().toString().equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.user_name));
                return false;
            }else if (phone.getText().toString().equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.mobile_number));
                return false;
            }else if (category.equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.choose_track_type));
                return false;
            }else if (notes.getText().toString().equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.notes));
                return false;
            }
            return true;
    }
}
