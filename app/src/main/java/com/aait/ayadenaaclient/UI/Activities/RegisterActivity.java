package com.aait.ayadenaaclient.UI.Activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.ayadenaaclient.App.Constant;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;

import com.aait.ayadenaaclient.UI.Views.ListDialog;
import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.aait.ayadenaaclient.Uitls.PermissionUtils;
import com.aait.ayadenaaclient.Uitls.ProgressRequestBody;
import com.aait.ayadenaaclient.Uitls.ValidationUtils;
import com.aait.ayadenaaclient.Base.ParentActivity;
import com.aait.ayadenaaclient.Listener.OnItemClickListener;
import com.aait.ayadenaaclient.Models.ListModel;
import com.aait.ayadenaaclient.Models.ListModelResponse;
import com.aait.ayadenaaclient.Models.RegisterResponse;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.ayadenaaclient.App.Constant.RequestPermission.REQUEST_IMAGES;

public class RegisterActivity extends ParentActivity implements OnItemClickListener,ProgressRequestBody.UploadCallbacks {

    @BindView(R.id.lay_splash)
    LinearLayout laySplash;

    @BindView(R.id.civ_profile_pic)
    CircleImageView civProfilePic;

    @BindView(R.id.til_name)
    TextInputLayout tilName;

    @BindView(R.id.et_name)
    TextInputEditText etName;

    @BindView(R.id.til_password)
    TextInputLayout tilPassword;

    @BindView(R.id.et_password)
    TextInputEditText etPassword;

    @BindView(R.id.til_confirm_password)
    TextInputLayout tilConfirmPassword;

    @BindView(R.id.et_confirm_password)
    TextInputEditText etConfirmPassword;

    @BindView(R.id.til_email)
    TextInputLayout tilEmail;

    @BindView(R.id.et_email)
    TextInputEditText etEmail;

    @BindView(R.id.til_phone)
    TextInputLayout tilPhone;

    @BindView(R.id.et_phone)
    TextInputEditText etPhone;

    @BindView(R.id.til_location)
    TextInputLayout tilLocation;

    @BindView(R.id.et_location)
    TextInputEditText etLocation;



    @BindView(R.id.et_address)
    TextInputEditText et_address;

    @BindView(R.id.til_address)
    TextInputLayout til_address;

    @BindView(R.id.tv_terms_and_conditions)
    TextView tvTermsAndConditions;
    ListDialog mListDialog;

    ArrayList<ListModel> mListModels;
    ArrayList<ListModel> mListModels1;

    ListModel cityModel;
    ListModel areaModel;

    // select image from callery
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;
    int nationalityorCity = 0;

    String mAdresse, mLang, mLat = null;
    String type;
    boolean cityisclicked = false;
    private static final int REQUEST_CODE = 4;
    private static final int REQUEST_CODE_MATISSE = 5;
    private String Type = null;

    public static void startActivity(AppCompatActivity mAppCompatActivity,String type) {
        Intent mIntent = new Intent(mAppCompatActivity, RegisterActivity.class);
        mIntent.putExtra("type",type);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }

    void getBundleData(){
        type = (String)getIntent().getSerializableExtra("type");
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    @Override
    protected void initializeComponents() {
       getBundleData();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {
        mListDialog.dismiss();

            cityModel = mListModels.get(position);
            etLocation.setText(cityModel.getName());



        CommonUtil.PrintLogE("City id : " + cityModel.getId());
    }


    @OnClick(R.id.et_location)
    void onAreaClick(){
        nationalityorCity = 0;
        getCities();


    }

    @OnClick(R.id.btn_register)
    void onBtnRegisterClick() {
        if (registerValidation()) {
            if (ImageBasePath!=null){
                Register(mLanguagePrefManager.getAppLanguage(),"1",etName.getText().toString(),etEmail.getText().toString(),
                        etPhone.getText().toString(),mLat,mLang,cityModel.getId()+"",etPassword.getText().toString(),ImageBasePath
                );
            }

        }
        //ConfirmCodeActivity.startActivity((AppCompatActivity)mContext,type);
    }

    @OnClick(R.id.civ_profile_pic)
    void onImageClick() {
//        getPickImageWithPermission();
        getFile();

    }
    @OnClick(R.id.et_address)
    void onAddressClick(){
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }

    @OnClick(R.id.tv_terms_and_conditions)
    void onTermsAndConditionClick() {
        //TODO implement
        TermsAndConditions.startActivity((AppCompatActivity) mContext);
    }
    boolean registerValidation() {
        if (ImageBasePath == null){
            CommonUtil.makeToast(mContext, getString(R.string.choose_your_avatar));
            return false;
        }
        else if (!ValidationUtils.checkError(etName, tilName, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etPhone, tilPhone, getString(R.string.fill_empty))) {
            return false;
        }else if (etPhone.getText().toString().length()<9){
            CommonUtil.makeToast(mContext,getString(R.string.phone_validation));
            return false;
        }
        else if (!ValidationUtils.checkError(etEmail, tilEmail, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.validateEmail(etEmail, tilEmail,getString(R.string.enter_your_email),getString(R.string.Email_is_not_in_correct_format))) {
            return false;
        }
        else if (!ValidationUtils.checkError(et_address,til_address,getString(R.string.fill_empty))){
            return false;
        }
        else if (!ValidationUtils.checkError(etLocation, tilLocation, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils.checkError(etPassword, tilPassword, getString(R.string.fill_empty))) {
            return false;
        } else if (!ValidationUtils
                .checkMatch(etPassword, etConfirmPassword, tilPassword, getString(R.string.fill_empty))) {
            return false;
        }
        return true;

    }

    private void getFile() {
        if (ActivityCompat.checkSelfPermission(RegisterActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE);
        } else {
            attach();
        }
    }
    private void attach() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select file to upload "), REQUEST_CODE_MATISSE);

    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        civProfilePic.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }

    private void getCities(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCities(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().issucessfull()){
                        mListModels = response.body().getData();
                        mListDialog = new ListDialog(mContext,RegisterActivity.this
                                ,mListModels,getString(R.string.city));
                        mListDialog.show();
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }


    private void Register(String lang,String type,String name,String email,String phone,
                          String lat,String lng,String City_id,String password ,String pathFromImage){
        showProgressDialog(getResources().getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(pathFromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, RegisterActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).clientRegister(lang,type,name,password,email,phone,City_id,lat,lng,filePart)
                .enqueue(new Callback<RegisterResponse>() {
                    @Override
                    public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()){
                            if (response.body().issucessfull()){
                                CommonUtil.makeToast(mContext,getString(R.string.enter_confirmation_code));
                                ConfirmCodeActivity.startActivity((AppCompatActivity)mContext,response.body().getData().getUser_id()+"");
                            }else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        hideProgressDialog();
                    }
                });

    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse);
                    et_address.setText(mAdresse);
                }
            }else   if (requestCode == REQUEST_CODE_MATISSE) {
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    Log.e("path", uri.toString());
                    String path = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        path = CommonUtil.getPath(this, uri);
                        Log.e("path", path);

                        Type = getContentResolver().getType(uri);
                        Log.e("type",Type);
                        if ((Type.startsWith("image"))){
                            ImageBasePath = CommonUtil.getPath(RegisterActivity.this,uri);

                            civProfilePic.setImageURI(Uri.parse(ImageBasePath));

                        }else {

                        }
                    }

                }

            }
        }
    }

}
