package com.aait.ayadenaaclient.UI.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.aait.ayadenaaclient.Base.ParentActivity;
import com.aait.ayadenaaclient.Models.TermsResponse;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsAndConditions extends ParentActivity {

    @BindView(R.id.tv_terms_and_conditions)
    TextView tvTermsAndConditions;

    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, TermsAndConditions.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.terms_and_conditions));
        getTermsAndConditions();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_terms_and_conditions;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getTermsAndConditions() {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getTerms(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<TermsResponse>() {
            @Override
            public void onResponse(Call<TermsResponse> call, Response<TermsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().issucessfull()){
                        tvTermsAndConditions.setText(response.body().getData());
                    }
                }
            }

            @Override
            public void onFailure(Call<TermsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });

    }
}
