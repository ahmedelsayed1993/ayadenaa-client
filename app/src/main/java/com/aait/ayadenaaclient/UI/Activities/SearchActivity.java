package com.aait.ayadenaaclient.UI.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.aait.ayadenaaclient.App.Constant;
import com.aait.ayadenaaclient.Models.PriceModel;
import com.aait.ayadenaaclient.Models.PriceResponse;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.UI.Adapters.OffersAdapter;
import com.aait.ayadenaaclient.UI.Views.ListDialog;

import com.aait.ayadenaaclient.UI.Views.ListDialog1;
import com.aait.ayadenaaclient.Uitls.CommonUtil;
import com.aait.ayadenaaclient.Base.ParentActivity;
import com.aait.ayadenaaclient.Listener.OnItemClickListener;
import com.aait.ayadenaaclient.Models.ListModel;
import com.aait.ayadenaaclient.Models.ListModelResponse;
import com.aait.ayadenaaclient.Models.ProductModel;
import com.aait.ayadenaaclient.Models.SearchResponse;

import com.aait.ayadenaaclient.Models.ShopsResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends ParentActivity implements OnItemClickListener {

    @BindView(R.id.ed_categories)
    EditText edCategories;

    @BindView(R.id.ed_city)
    EditText edCity;


    @BindView(R.id.ed_prices)
    EditText edPrices;

    @BindView(R.id.iv_location)
    ImageView ivLocation;
    boolean cityIsChecked = false;

    ArrayList<ListModel> mListModels;


    ListDialog mListDialog;
    ListDialog1 mListDialog1;

    ListModel categoresModel = null;

    ListModel cityModel = null;



    PriceModel priceModel = null;
    ArrayList<PriceModel> priceModels ;



    int listDialogType;


    // recycle items
    LinearLayoutManager linearLayoutManager;

    OffersAdapter mOffersAdapter;

    List<ProductModel> mFoodModels = new ArrayList<>();

    private static final int PAGE_START = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;

    String mAdresse, mLang, mLat = null;
    String text;


    public static void startActivity(AppCompatActivity mAppCompatActivity) {
        Intent mIntent = new Intent(mAppCompatActivity, SearchActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAppCompatActivity.startActivity(mIntent);
    }
    @Override
    protected void initializeComponents() {
        setToolbarTitle(getString(R.string.search));

        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mOffersAdapter = new OffersAdapter(mContext, mFoodModels);
        mOffersAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setItemAnimator(new DefaultItemAnimator());
        rvRecycle.setAdapter(mOffersAdapter);
        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        if (mSharedPrefManager.getLoginStatus()) {
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Search(mSharedPrefManager.getUserData().getUser_id(), 0, 0,  0, "", "");

                }
            });

            Search(mSharedPrefManager.getUserData().getUser_id(), 0, 0,  0, "", "");
        }else {
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Search(0, 0, 0, 0,  "", "");

                }
            });

            Search(0, 0, 0, 0,  "", "");
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_search;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(View view, int position) {
        if (mSharedPrefManager.getLoginStatus()) {
            if (view.getId() == R.id.tv_row_title) {
                mListDialog.dismiss();
                if (listDialogType == Constant.SearchKeys.categories) {
                    categoresModel = mListModels.get(position);
                    edCategories.setText(categoresModel.getName());
                    Search(mSharedPrefManager.getUserData().getUser_id(), categoresModel.getId(), 0, 0,  "", "");
                    CommonUtil.PrintLogE("cat id : " + categoresModel.getId());
                } else if (listDialogType == Constant.SearchKeys.city) {
                    cityModel = mListModels.get(position);
                    edCity.setText(cityModel.getName());

                    //getAreas(mLanguagePrefManager.getAppLanguage(),cityModel.getId());
                    Search(mSharedPrefManager.getUserData().getUser_id(), 0, cityModel.getId(),  0, "","");

                    cityIsChecked = true;

                    CommonUtil.PrintLogE("city id : " + cityModel.getId());
                }
            }

            else if (view.getId() == R.id.tv_row_title1){
                mListDialog1.dismiss();
                if (listDialogType == Constant.SearchKeys.price) {
                    priceModel = priceModels.get(position);
                    edPrices.setText(priceModel.getFrom()+"-"+priceModel.getTo());
                    Search(mSharedPrefManager.getUserData().getUser_id(), 0, 0, priceModel.getId(),  "", "");
                    CommonUtil.PrintLogE("price id : " + priceModel.getId());

                }
            }else {
                Intent intent = new Intent(mContext,OrderItemActivity.class);
                intent.putExtra("id",mFoodModels.get(position).getProduct_id()+"");
                startActivity(intent);
            }
        }else {
            if (view.getId() == R.id.tv_row_title) {
                mListDialog.dismiss();
                if (listDialogType == Constant.SearchKeys.categories) {
                    categoresModel = mListModels.get(position);
                    edCategories.setText(categoresModel.getName());
                    Search(0, categoresModel.getId(), 0,  0, "", "");
                    CommonUtil.PrintLogE("cat id : " + categoresModel.getId());
                } else if (listDialogType == Constant.SearchKeys.city) {
                    cityModel = mListModels.get(position);
                    edCity.setText(cityModel.getName());

                    //getAreas(mLanguagePrefManager.getAppLanguage(),cityModel.getId());
                    Search(0, 0, cityModel.getId(), 0, "", "");



                    CommonUtil.PrintLogE("city id : " + cityModel.getId());
                }
            }
                else if (view.getId() == R.id.tv_row_title1){
                mListDialog1.dismiss();
                    if (listDialogType == Constant.SearchKeys.price) {
                        priceModel = priceModels.get(position);
                        edPrices.setText(priceModel.getFrom()+"-"+priceModel.getTo());
                        Search(0, 0, 0, priceModel.getId(),  "", "");
                        CommonUtil.PrintLogE("price id : " + priceModel.getId());

                    }
                }
             else {
                Intent intent = new Intent(mContext,OrderItemActivity.class);
                intent.putExtra("id",mFoodModels.get(position).getProduct_id()+"");
                startActivity(intent);
            }
        }
    }

    @OnClick(R.id.iv_location)
    void onAdressSearch() {
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity) mContext);
    }

    @OnClick(R.id.ed_categories)
    void onCategoriesClick() {
        listDialogType = Constant.SearchKeys.categories;
        getCategories();
    }

    @OnClick(R.id.ed_city)
    void onCityClick() {
        listDialogType = Constant.SearchKeys.city;
        getCities();
    }



    @OnClick(R.id.ed_prices)
    void onPricesClick() {
        listDialogType = Constant.SearchKeys.price;
        getPriceList();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mSharedPrefManager.getLoginStatus()) {
            if (data != null) {
                if (requestCode == Constant.RequestCode.GET_LOCATION) {
                    if (resultCode == RESULT_OK) {
                        mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                        mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                        mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                        Search(mSharedPrefManager.getUserData().getUser_id(), 0, 0,  0, mLat, mLang);
                    }
                }
            }
        }else {
            if (data != null) {
                if (requestCode == Constant.RequestCode.GET_LOCATION) {
                    if (resultCode == RESULT_OK) {
                        mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                        mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                        mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                        Search(0, 0, 0,  0, mLat, mLang);
                    }
                }
            }
        }
    }
    private void getCategories(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCategories(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mListModels =response.body().getData();
                        mListDialog = new ListDialog(mContext,SearchActivity.this,mListModels,getString(R.string.categories));
                        mListDialog.show();
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    private void getCities(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCities(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListModelResponse>() {
            @Override
            public void onResponse(Call<ListModelResponse> call, Response<ListModelResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        mListModels =response.body().getData();
                        mListDialog = new ListDialog(mContext,SearchActivity.this,mListModels,getString(R.string.city));
                        mListDialog.show();
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListModelResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    private void getPriceList(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getPriceList(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<PriceResponse>() {
            @Override
            public void onResponse(Call<PriceResponse> call, Response<PriceResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        priceModels = response.body().getData();
                        mListDialog1 = new ListDialog1(mContext,SearchActivity.this,priceModels,getString(R.string.prices_range));
                        mListDialog1.show();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<PriceResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    private void Search(int userId,int catId,int cityId,int priceId,String lat,String lng){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).search(mLanguagePrefManager.getAppLanguage(),userId,catId,cityId,priceId,lat,lng).enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getStatus()==1){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.no_data);
                        }else {
                            mOffersAdapter.updateAll(response.body().getData());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });
    }

}
