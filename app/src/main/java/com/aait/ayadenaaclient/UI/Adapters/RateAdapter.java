package com.aait.ayadenaaclient.UI.Adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatRatingBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaclient.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaclient.Models.CommentsModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class RateAdapter extends ParentRecyclerAdapter<CommentsModel> {

    public RateAdapter(final Context context, final List<CommentsModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        CommentsModel rateModel = data.get(position);
        Glide.with(mcontext).load(rateModel.getComment_user_img()).asBitmap().skipMemoryCache(true)
                .placeholder(R.mipmap.splash)
                .into(viewHolder.civImage);
        viewHolder.tvName.setText(rateModel.getComment_username());
        viewHolder.tvRate.setText(rateModel.getComment());
        viewHolder.tvDate.setText(rateModel.getComment_date());
       // viewHolder.ratingBar.setRating(Float.parseFloat(rateModel.getRate()));

    }


    public class ViewHolder extends ParentRecyclerViewHolder {


        @BindView(R.id.civ_image)
        CircleImageView civImage;

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.tv_date)
        TextView tvDate;

        @BindView(R.id.tv_rate)
        TextView tvRate;



        ViewHolder(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }

    }
}

