package com.aait.ayadenaaclient.UI.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.aait.ayadenaaclient.App.Constant.BundleData;
import com.aait.ayadenaaclient.Network.RetroWeb;
import com.aait.ayadenaaclient.Network.ServiceApi;
import com.aait.ayadenaaclient.R;
import com.aait.ayadenaaclient.UI.Adapters.FamilyAdapter;
import com.aait.ayadenaaclient.Base.ParentActivity;
import com.aait.ayadenaaclient.Listener.OnItemClickListener;
import com.aait.ayadenaaclient.Listener.PaginationAdapterCallback;
import com.aait.ayadenaaclient.Listener.PaginationScrollListener;
import com.aait.ayadenaaclient.Models.ShopModel;
import com.aait.ayadenaaclient.Models.ShopsResponse;
import com.aait.ayadenaaclient.Uitls.CommonUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */

public class FamiliesActivity extends ParentActivity implements OnItemClickListener, PaginationAdapterCallback {


    LinearLayoutManager linearLayoutManager;

    FamilyAdapter mFamilyAdapter;

   ArrayList<ShopModel> shopModels = new ArrayList<>();

    private static final int PAGE_START = 1;

    private boolean isLoading = false;

    private boolean isLastPage = false;

    private int TOTAL_PAGES;

    private int currentPage = PAGE_START;

    int CategoryID;
    String id;
    String name;


    @Override
    protected void initializeComponents() {
        id = getIntent().getStringExtra("id");
        name = getIntent().getStringExtra("name");
        Log.e("id",id);
        setToolbarTitle(name);


        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvRecycle.setLayoutManager(linearLayoutManager);
        mFamilyAdapter = new FamilyAdapter(mContext, shopModels);
        mFamilyAdapter.setOnItemClickListener(this);
        mFamilyAdapter.setOnPaginationClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setItemAnimator(new DefaultItemAnimator());
        rvRecycle.setAdapter(mFamilyAdapter);
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

        });
        if (mSharedPrefManager.getLoginStatus()) {
            swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getFamilies(Integer.parseInt(id), mSharedPrefManager.getUserData().getUser_id());

                }
            });

            getFamilies(Integer.parseInt(id), mSharedPrefManager.getUserData().getUser_id());
        }else {
            swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getFamilies(Integer.parseInt(id), 0);

                }
            });

            getFamilies(Integer.parseInt(id), 0);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_recycle;
    }

    @Override
    protected boolean isEnableToolbar() {
        return true;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return true;
    }

    @Override
    public void onItemClick(final View view, final int position) {
        Intent intent = new Intent(mContext,FamilyDetailsActivity.class);
        intent.putExtra("id",shopModels.get(position).getShop_id()+"");
        startActivity(intent);
       // FamilyDetailsActivity.startActivity((AppCompatActivity) mContext, mFamilyModels.get(position));
    }

    @Override
    public void retryPageLoad() {

    }

    private void getFamilies(int id,int user_id) {
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getShops(mLanguagePrefManager.getAppLanguage(), user_id,id)
                .enqueue(new Callback<ShopsResponse>() {
                    @Override
                    public void onResponse(Call<ShopsResponse> call,
                            Response<ShopsResponse> response) {
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                        if (response.isSuccessful()) {
                            if (response.body().getStatus()==1) {
                                assert response.body() != null;
//                            if (response.body().getData().size() == 0) {
//                                layNoItem.setVisibility(View.VISIBLE);
//                                layNoInternet.setVisibility(View.GONE);
//                                tvNoContent.setText(R.string.no_data);
//                            } else {
                                mFamilyAdapter.updateAll(response.body().getData());
                                // }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ShopsResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext, t);
                        t.printStackTrace();
                        layNoInternet.setVisibility(View.VISIBLE);
                        layNoItem.setVisibility(View.GONE);
                        layProgress.setVisibility(View.GONE);
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }

}
