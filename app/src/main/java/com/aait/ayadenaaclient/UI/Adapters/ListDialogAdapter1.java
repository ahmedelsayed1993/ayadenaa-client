package com.aait.ayadenaaclient.UI.Adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.ayadenaaclient.Base.ParentRecyclerAdapter;
import com.aait.ayadenaaclient.Base.ParentRecyclerViewHolder;
import com.aait.ayadenaaclient.Models.ListModel;
import com.aait.ayadenaaclient.Models.PriceModel;
import com.aait.ayadenaaclient.R;

import java.util.List;

import butterknife.BindView;

public class ListDialogAdapter1 extends ParentRecyclerAdapter<PriceModel> {

    public ListDialogAdapter1(final Context context, final List<PriceModel> data) {
        super(context, data);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        View viewItem = inflater.inflate(R.layout.rcycler_row, parent, false);
        viewHolder = new ListDialogAdapter1.ListAdapter(viewItem);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        ListDialogAdapter1.ListAdapter listAdapter = (ListDialogAdapter1.ListAdapter) holder;
        PriceModel listModel = data.get(position);
        listAdapter.tv_row_title.setText(listModel.getFrom()+"-"+listModel.getTo());
        if (position % 2 == 0) {
            listAdapter.tv_row_title.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorRowSecondary));
        } else {
            listAdapter.tv_row_title.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorRowPrimary));

        }

        listAdapter.tv_row_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                itemClickListener.onItemClick(view, position);
            }
        });
    }


    protected class ListAdapter extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_row_title1)
        TextView tv_row_title;

        public ListAdapter(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }
}

