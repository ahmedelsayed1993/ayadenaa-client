package com.aait.ayadenaaclient.Listener;

import android.view.View;

public interface OnItemClickListener {
    public void onItemClick(View view, int position);
}
